// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerPlacement.h"
#include "Components/ArrowComponent.h"

// Sets default values
ATowerPlacement::ATowerPlacement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow Component"));

}

// Called when the game starts or when spawned
void ATowerPlacement::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATowerPlacement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

