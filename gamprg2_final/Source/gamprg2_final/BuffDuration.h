// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Buff.h"
#include "BuffDuration.generated.h"

/**
 * 
 */
UCLASS()
class GAMPRG2_FINAL_API UBuffDuration : public UBuff
{
	GENERATED_BODY()


public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float duration;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float currentDuration;

	UFUNCTION()
		void ResetDuration();

	
	void Tick_Implementation(UBuffReceiver* buffreceiver, float DeltaTime) override;
	
};
