// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffReceiver.h"
#include "Buff.h"

// Sets default values for this component's properties
UBuffReceiver::UBuffReceiver()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void UBuffReceiver::AddBuff(TSubclassOf<UBuff> buff)
{
	if (buff == nullptr || ContainsBuff(buff)) {
		return;
	}
	GEngine->AddOnScreenDebugMessage(-1, 50, FColor::Red, "Add Buff");
	UBuff* ref = NewObject<UBuff>(this, buff);
	buffList.Add(ref);
	ref->OnReceiveBuff(this);

	
		
	
}

void UBuffReceiver::RemoveBuff(TSubclassOf<UBuff> buff)
{
	if (buff == nullptr || !ContainsBuff(buff)) {
		return;
	}
	UBuff* ref = GetBuff(buff);
	buffList.Remove(ref);
	ref->OnRemoveBuff(this);
}

bool UBuffReceiver::ContainsBuff(TSubclassOf<UBuff> buff)
{
	for (UBuff* inList : buffList) {
		if (inList->IsA(UBuff::StaticClass())) {
			return true;
		}
	}
	return nullptr;
}

UBuff* UBuffReceiver::GetBuff(TSubclassOf<UBuff> buff)
{
	for (UBuff* inList : buffList) {
		if (inList->IsA(UBuff::StaticClass())) {
			return inList;
		}
	}
	return false;
	
}

// Called when the game starts
void UBuffReceiver::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UBuffReceiver::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

