// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyController.h"
#include "Enemy.h"
#include "Engine/TargetPoint.h"
#include "Engine/TargetPoint.h"
#include "Kismet/KismetSystemLibrary.h"



void AEnemyController::BeginPlay()
{
	Super::BeginPlay();


}

void AEnemyController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!IsMoving && Waypoints.Num() > 0)
	{
		StartMoving();
	}
}

void AEnemyController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	if (Index + 1 >= Waypoints.Num())
	{
		GEngine->AddOnScreenDebugMessage(-1, 50, FColor::Red, "Move Completed");
		return;
	}

	Index++;
	StartMoving();
}

void AEnemyController::StartMoving()
{
	if (Waypoints[Index] == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 50, FColor::Red, "Error on Start Moveing");
		return;
	}

	IsMoving = true;
	MoveToActor(Waypoints[Index], WaypointAcceptanceRadius);
}
