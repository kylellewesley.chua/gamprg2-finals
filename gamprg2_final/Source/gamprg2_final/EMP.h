// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "EMP.generated.h"

/**
 * 
 */

class USphereComponent;
class UBuff;

UCLASS()
class GAMPRG2_FINAL_API AEMP : public ATower
{
	GENERATED_BODY()
	

	public:
		AEMP();

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			USphereComponent* SphereComponent;

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			TSubclassOf<UBuff> slowSpeedBuff;


		UFUNCTION()
			void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

		UFUNCTION()
			void EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

};
