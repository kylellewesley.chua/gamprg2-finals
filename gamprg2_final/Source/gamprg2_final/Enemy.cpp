// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "HealthComponent.h"
#include "BuffReceiver.h"
#include "Kismet/GameplayStatics.h"
#include "ToweDefenserController.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Heath Component"));
	BuffReceiver = CreateDefaultSubobject<UBuffReceiver>(TEXT("Buff Receiver"));
	

}

void AEnemy::GiveMoneyToPlayer()
{
	if (hasReachCore) {
		Destroy();
		return;
	}

	AToweDefenserController* controller = Cast<AToweDefenserController>(UGameplayStatics::GetPlayerController(this, 0));
	if (controller == nullptr) {
		return;
	}
	controller->AddMoney(moneyDrop);
	Destroy();
}

void AEnemy::ReachPlayerCore()
{
	hasReachCore = true;
	HealthComponent->TakeDamage(HealthComponent->health);
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	HealthComponent->OnDie.AddDynamic(this, &AEnemy::GiveMoneyToPlayer);

	switch (gait) {

		case EEnemyGait::Flying:
			GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);
			break;

		default:
			GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
			break;
	}
	
	
	
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

