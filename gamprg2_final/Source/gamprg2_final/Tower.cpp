// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Enemy.h"
#include "Projectile.h"
#include "BuffReceiver.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StaticMeshTurretBase = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tower Base Mesh"));
	StaticMeshTurretHead = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tower Head Mesh"));
	BuffReceiver = CreateDefaultSubobject<UBuffReceiver>(TEXT("Buff Receiver"));
	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	StaticMeshTurretHead->SetupAttachment(StaticMeshTurretBase, TEXT("HeadSocket"));
	Arrow->SetupAttachment(StaticMeshTurretHead);
	
	

	

}



AEnemy* ATower::TargetEnemy()
{
	TArray<FHitResult> result;
	TArray<AActor*> actor;
	bool hasHit = UKismetSystemLibrary::SphereTraceMulti(this, GetActorLocation(), GetActorLocation(), attackRange, query, false, actor, EDrawDebugTrace::ForOneFrame, result, true);
	if (!hasHit) {
		return nullptr;
	}

	float shortestDistance = 100000000000000000;
	AEnemy* enemy = nullptr;

	for (FHitResult targetHit : result) {
		AEnemy* hitActor = Cast<AEnemy>(targetHit.Actor.Get());
		if (hitActor == nullptr) {
			continue;
		}		
		

		float distance = FVector::Distance(hitActor->GetActorLocation(), this->GetActorLocation());

		if (distance < shortestDistance) {
			shortestDistance = distance;
			enemy = hitActor;
		}
	}
	return enemy;
}

void ATower::ShootProjectile_Implementation(FVector location)
{
	
	FRotator projectileRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), location);
	AProjectile* spawnProjectile = GetWorld()->SpawnActor<AProjectile>(projectile, Arrow->GetComponentLocation() , projectileRotation);
	
	

}

void ATower::CheckForEnemy(float DeltaTime)
{
	if (shootCounter <= fireRate) {
		shootCounter += DeltaTime;
	}

	if (enemyAcquired == nullptr) {
		enemyAcquired = TargetEnemy();
	}
	else {
		float distance = FVector::Distance(enemyAcquired->GetActorLocation(), this->GetActorLocation());
		if (distance > attackRange) {
			enemyAcquired = nullptr;
			return;
		}

		FRotator projectileRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), enemyAcquired->GetActorLocation());
		projectileRotation.Pitch = 0;
		projectileRotation.Roll = 0;
		StaticMeshTurretHead->SetWorldRotation(projectileRotation);

		if (shootCounter > fireRate && enemyAcquired != nullptr) {
			ShootProjectile(enemyAcquired->GetActorLocation());
			shootCounter = 0;
		}
	}
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (shouldFire) {
		CheckForEnemy(DeltaTime);
	}
	
	
	

}

