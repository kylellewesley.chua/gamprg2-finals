// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyTypes.h"
#include "Enemy.generated.h"

class ATargetPoint;
class UHealthComponent;
class UBuffReceiver;
class UCharacterMovementComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath) ;

UCLASS()
class GAMPRG2_FINAL_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();


	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		EEnemyTypes type;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		EEnemyGait  gait;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 moneyDrop = 10;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 damage = 10;

	bool hasReachCore = false;


	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UHealthComponent* HealthComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UBuffReceiver* BuffReceiver;

	UFUNCTION()
		void GiveMoneyToPlayer();

	UFUNCTION()
		void ReachPlayerCore();

	UPROPERTY(BlueprintAssignable)
		FOnDeath onDeath;

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	

	


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
