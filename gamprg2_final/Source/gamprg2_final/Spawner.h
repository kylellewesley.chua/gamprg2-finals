// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

class ATargetPoint;
class USpawnerInfo;
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnClearWave);


UCLASS()
class GAMPRG2_FINAL_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<ATargetPoint*> waypoints;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Wave Dictionary")
		TMap<int32, USpawnerInfo*> WaveDictionary;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 enemyScaler;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 healthMultiplier;



	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		ATargetPoint* spawnLocation;

	UPROPERTY(BlueprintReadWrite)
		float spawnDelay = 1.0f;

	UPROPERTY(BlueprintAssignable)
		FOnClearWave onClearWave;

	USpawnerInfo* spawnerInfo;

	int32 enemyIndex = 0;

	int32 enemyDeathCount = 0;


	FTimerHandle spawnHandle;

	UFUNCTION(BlueprintCallable)
		void InitializeSpawn(int32 wave);

	UFUNCTION()
		void Spawn();

	UFUNCTION()
		void EnemyDeaths();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
