// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/KismetSystemLibrary.h"
#include "ToweDefenserController.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMoneySignature, int32, value);

UENUM(BlueprintType)
enum class EPlayerState :uint8 {
	SELECTING,
	PLACING,
	UPGRADING
};

class ATowerGhost;
class UArrowComponent;
class ATower;
class UHealthComponent;

UCLASS(Blueprintable)
class GAMPRG2_FINAL_API AToweDefenserController : public APlayerController
{
	GENERATED_BODY()
public:

	AToweDefenserController();

		EPlayerState state;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TEnumAsByte<ETraceTypeQuery> query;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<ATower> towerToSpawn;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 currency;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UHealthComponent* HealthComponent;


	bool canPlace = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<ATowerGhost> towerGhostClass;

	TWeakObjectPtr<ATowerGhost> towerGhost;

	UFUNCTION()
		void PlayerClick();
	
	UFUNCTION()
		void HandlePlayerState();

	UFUNCTION()
		void PlaceTower();

	UFUNCTION()
		void UpdateTowerGhost();

	UFUNCTION()
		FHitResult GetMousePostionFromWorld(bool& hasHit);

	UFUNCTION(BlueprintCallable)
		void StartPlacing(TSubclassOf<ATower> tower);

	UFUNCTION()
		void AddMoney(int32 value);

	UFUNCTION()
		void SubtractMoney(int32 value);

	UFUNCTION()
		void SetState(EPlayerState nextState);

	UFUNCTION()
		bool CanSpendMoney(int32 value);

	UFUNCTION(BlueprintPure)
		int32 GetCurrency() {
		return currency;
	}

	UPROPERTY(BlueprintAssignable)
		FMoneySignature OnAddMoney;

	UPROPERTY(BlueprintAssignable)
		FMoneySignature OnSubtractMoney;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetupInputComponent() override;
	
};
