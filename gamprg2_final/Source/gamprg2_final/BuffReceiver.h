// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BuffReceiver.generated.h"

class UBuff;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMPRG2_FINAL_API UBuffReceiver : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBuffReceiver();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<UBuff*> buffList;

	UFUNCTION(BlueprintCallable)
		void AddBuff(TSubclassOf<UBuff> buff);

	UFUNCTION(BlueprintCallable)
		void RemoveBuff(TSubclassOf<UBuff> buff);

	UFUNCTION(BlueprintPure)
		bool ContainsBuff(TSubclassOf<UBuff> buff);

	UFUNCTION(BlueprintPure)
		UBuff* GetBuff(TSubclassOf<UBuff> buff);


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
