// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "Engine/TargetPoint.h"
#include "SpawnerInfo.h"
#include "Enemy.h"
#include "EnemyController.h"
#include "HealthComponent.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawner::InitializeSpawn(int32 wave) {

	

	if (!WaveDictionary.Contains(wave)) {
		return;
	}

	enemyScaler = wave;
	spawnerInfo = WaveDictionary[wave];
	enemyIndex = 0;
	enemyDeathCount = 0;
	
	GetWorld()->GetTimerManager().SetTimer(spawnHandle, this, &ASpawner::Spawn, spawnDelay);

}

void ASpawner::Spawn()
{
	
	auto enemy = GetWorld()->SpawnActor<AEnemy>(spawnerInfo->enemyList[enemyIndex], spawnLocation->GetActorLocation(), spawnLocation->GetActorRotation());
	auto controller = Cast<AEnemyController>(enemy->GetController());
	controller->Waypoints = waypoints;

	enemyIndex++;
	if (spawnerInfo->enemyList.Num() - 1 >= enemyIndex) {
		GetWorld()->GetTimerManager().SetTimer(spawnHandle, this, &ASpawner::Spawn, spawnDelay);
	}
	enemy->HealthComponent->health += enemyScaler * healthMultiplier;
	enemy->HealthComponent->OnDie.AddDynamic(this, &ASpawner::EnemyDeaths);
	


}

void ASpawner::EnemyDeaths()
{
	enemyDeathCount++;

	if (spawnerInfo->enemyList.Num() >= enemyDeathCount) {
		onClearWave.Broadcast();
	}

	

}

