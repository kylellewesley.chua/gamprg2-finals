// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerDefenseGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Spawner.h"
#include "Enemy.h"

void ATowerDefenseGameMode::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> out;

	UGameplayStatics::GetAllActorsOfClass(this, ASpawner::StaticClass(), out);

	for (AActor* actor : out) {

		ASpawner* spawner = Cast<ASpawner>(actor);

		if (spawner == nullptr) {
			continue;
		}

		spawner->onClearWave.AddDynamic(this, &ATowerDefenseGameMode::WaveCount);
		spawnerList.Add(spawner);
	}

}

void ATowerDefenseGameMode::StartWave()
{
	currentWave++;
	for (ASpawner* spawner : spawnerList) {
		spawner->InitializeSpawn(GetCurrentWave());
	}
	OnStartWave.Broadcast();
}

void ATowerDefenseGameMode::WaveCount()
{
	waveCount++;

	if (spawnerList.Num() >= waveCount) {
		OnEndWave.Broadcast();
		waveCount = 0;
		//currentWave++;
		GetWorld()->GetTimerManager().SetTimer(waveHandle, this, &ATowerDefenseGameMode::StartWave, waveDelay);
	
	}


}
