// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyController.generated.h"

/**
 * 
 */
class AEnemy;
class ATargetPoint;

UCLASS()
class GAMPRG2_FINAL_API AEnemyController : public AAIController
{
	GENERATED_BODY()

public:


	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AI Behaviour : Waypoints")
		float WaypointAcceptanceRadius = 50;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AI Behaviour : Waypoints")
		TArray<ATargetPoint*> Waypoints;

protected:

	int32 Index;
	bool IsMoving = false;

protected:	

		virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

	UFUNCTION(BlueprintCallable)
		void StartMoving();

};
