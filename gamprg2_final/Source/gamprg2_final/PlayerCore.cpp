// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCore.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Enemy.h"
#include "ToweDefenserController.h"
#include "HealthComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
APlayerCore::APlayerCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StaticMeshCore = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	
	BoxComponent->SetupAttachment(StaticMeshCore);

}

void APlayerCore::CoreCollision(AEnemy* enemy)
{
	AToweDefenserController* controller = Cast<AToweDefenserController>(UGameplayStatics::GetPlayerController(this, 0));
	if (enemy == nullptr) {
		return;
	}
	controller->HealthComponent->TakeDamage(enemy->damage);
	

}

void APlayerCore::OnHit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AEnemy* enemy = Cast<AEnemy>(OtherActor);
	if (enemy == nullptr) {
		return;
	}
	CoreCollision(enemy);
	enemy->ReachPlayerCore();
}

// Called when the game starts or when spawned
void APlayerCore::BeginPlay()
{
	Super::BeginPlay();
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &APlayerCore::OnHit);
	
}

// Called every frame
void APlayerCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

