// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "SpawnerInfo.generated.h"


/**
 * 
 */

class AEnemy;

UCLASS()
class GAMPRG2_FINAL_API USpawnerInfo : public UDataAsset
{
	GENERATED_BODY()

	public:
		UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<TSubclassOf<AEnemy>> enemyList;

		
		
	




};
