// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Tower.generated.h"


class AProjectile;
class AEnemy;
class UStaticMeshComponent;
class UArrowComponent;
class UBuffReceiver;

UCLASS()
class GAMPRG2_FINAL_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float fireRate = 0.5f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float attackRange = 10.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 damage = 10;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 cost = 10;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool shouldFire = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UBuffReceiver* BuffReceiver;

		float shootCounter = 0;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		UStaticMeshComponent* StaticMeshTurretBase;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		UStaticMeshComponent* StaticMeshTurretHead;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		UArrowComponent* Arrow;


	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TEnumAsByte<ETraceTypeQuery> query;


	TWeakObjectPtr<AEnemy> enemyAcquired;

	

	bool isAttacking;


	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TSubclassOf<AProjectile> projectile;
	
	
	UFUNCTION()
		AEnemy* TargetEnemy();

	UFUNCTION(BlueprintNativeEvent)
		void ShootProjectile(FVector location);
		virtual void ShootProjectile_Implementation(FVector location);

		UFUNCTION()
			void CheckForEnemy(float DeltaTime);

	


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
