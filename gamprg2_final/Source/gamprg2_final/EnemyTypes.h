// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyTypes.generated.h"


/**
 * 
 */

UENUM(BlueprintType)
enum class EEnemyTypes : uint8
{
	Normal,
	Boss
};

UENUM(BlueprintType)
enum class EEnemyGait : uint8
{
	Grounded,
	Flying
};

