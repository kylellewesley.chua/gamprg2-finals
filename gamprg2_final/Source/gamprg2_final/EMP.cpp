// Fill out your copyright notice in the Description page of Project Settings.


#include "EMP.h"
#include "Components/SphereComponent.h"
#include "Enemy.h"
#include "BuffReceiver.h"
#include "Buff.h"


AEMP::AEMP() {
	PrimaryActorTick.bCanEverTick = true;

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Component"));

}

void AEMP::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AEnemy* enemy = Cast<AEnemy>(OtherActor);

	if (enemy == nullptr) {
		return;
	}
	enemy->BuffReceiver->AddBuff(slowSpeedBuff);

}


void AEMP::EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AEnemy* enemy = Cast<AEnemy>(OtherActor);
	if (enemy == nullptr) {
		return;
	}
	enemy->BuffReceiver->RemoveBuff(slowSpeedBuff);
}

void AEMP::BeginPlay() 
{
	Super::BeginPlay();
	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AEMP::OnOverlap);
	SphereComponent->OnComponentEndOverlap.AddDynamic(this, &AEMP::EndOverlap);
}

