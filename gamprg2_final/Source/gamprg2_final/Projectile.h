// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"


class UStaticMeshComponent;
class UProjectileMovementComponent;

UCLASS(Blueprintable)
class GAMPRG2_FINAL_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		UStaticMeshComponent* StaticMeshProjectile;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		UProjectileMovementComponent* ProjectileMovement;


	UFUNCTION(BlueprintNativeEvent)
	void OnCollideProjectile(AActor* actor);
	virtual void OnCollideProjectile_Implementation(AActor* actor);

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
