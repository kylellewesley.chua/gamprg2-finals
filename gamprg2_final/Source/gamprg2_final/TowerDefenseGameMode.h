// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TowerDefenseGameMode.generated.h"

/**
 * 
 */
class ASpawner;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEndWaveSignature);
UCLASS()
class GAMPRG2_FINAL_API ATowerDefenseGameMode : public AGameModeBase
{
	GENERATED_BODY()


	int32 currentWave;

	TArray<ASpawner*> spawnerList;

	FTimerHandle waveHandle;

	protected:

		virtual void BeginPlay() override;

	public:

		int32 waveCount;

		UFUNCTION(BlueprintPure)
			FORCEINLINE int32 GetCurrentWave() const 
			{
				return currentWave; 
			}

		UFUNCTION(BlueprintCallable)
			void StartWave();

		UFUNCTION()
			void WaveCount();

		UPROPERTY(BlueprintReadWrite, EditAnywhere)
			float waveDelay = 2.0f;


		UPROPERTY(BlueprintAssignable)
			FOnEndWaveSignature OnEndWave;

		UPROPERTY(BlueprintAssignable)
			FOnEndWaveSignature OnStartWave;
};
