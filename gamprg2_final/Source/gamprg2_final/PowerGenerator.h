// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "PowerGenerator.generated.h"

class USphereComponent;
class UBuff;

/**
 * 
 */
UCLASS()
class GAMPRG2_FINAL_API APowerGenerator : public ATower
{
	GENERATED_BODY()

public:
	APowerGenerator();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USphereComponent* SphereComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TSubclassOf<UBuff> attackSpeedBuff;


	UFUNCTION()
		void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
