// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerGenerator.h"
#include "Components/SphereComponent.h"
#include "Tower.h"
#include "BuffReceiver.h"
#include "Buff.h"

APowerGenerator::APowerGenerator() {
	PrimaryActorTick.bCanEverTick = true;

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Component"));
}

void APowerGenerator::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ATower* tower = Cast<ATower>(OtherActor);
	if (tower == nullptr) {
		return;
	}
	tower->BuffReceiver->AddBuff(attackSpeedBuff);
}

void APowerGenerator::BeginPlay()
{
	Super::BeginPlay();
	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &APowerGenerator::OnOverlap);
}
