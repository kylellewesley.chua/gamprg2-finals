// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BuffTypes.h"
#include "Buff.generated.h"

/**
 * 
 */
class UBuffReceiver;

UCLASS(Blueprintable)
class GAMPRG2_FINAL_API UBuff : public UObject
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool shouldTick = false;


	UFUNCTION(BlueprintNativeEvent)
		void OnReceiveBuff(UBuffReceiver* buffreceiver);
	virtual void OnReceiveBuff_Implementation(UBuffReceiver* buffreceiver);

	UFUNCTION(BlueprintNativeEvent)
		void OnRemoveBuff(UBuffReceiver* buffreceiver);
		virtual void OnRemoveBuff_Implementation(UBuffReceiver* buffreceiver);

	UFUNCTION(BlueprintNativeEvent)
		void Tick(UBuffReceiver* buffreceiver, float DeltaTime);
	    virtual void Tick_Implementation(UBuffReceiver* buffreceiver, float DeltaTime);
	
};
