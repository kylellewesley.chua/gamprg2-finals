// Fill out your copyright notice in the Description page of Project Settings.


#include "ToweDefenserController.h"
#include "TowerPlacement.h"
#include "TowerGhost.h"
#include "Tower.h"
#include "HealthComponent.h"
#include "Components/ArrowComponent.h"

AToweDefenserController::AToweDefenserController() {

	PrimaryActorTick.bCanEverTick = true;
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Heath Component"));

}


void AToweDefenserController::PlayerClick()
{
	switch (state) {
	case EPlayerState::PLACING:
		PlaceTower();
		
		
		break;
	default:
		break;
	}
}

void AToweDefenserController::HandlePlayerState()
{
	switch (state) {
	case EPlayerState::PLACING:
		UpdateTowerGhost();
		break;
	default:
		break;
	}
}

void AToweDefenserController::PlaceTower()
{
	ATower* towerPrefab = towerToSpawn.GetDefaultObject();
	if (towerGhost == nullptr) {
		return;
	}
	if (canPlace) {
		GetWorld()->SpawnActor<ATower>(towerToSpawn, towerGhost->GetActorLocation(), towerGhost->GetActorRotation());
		SubtractMoney(towerPrefab->cost);
	}
	towerGhost->Destroy();
	
	SetState(EPlayerState::SELECTING);

}

void AToweDefenserController::UpdateTowerGhost()
{
	if (towerGhost == nullptr) {
		return;
	}
	bool hasHit;
	FHitResult hit = GetMousePostionFromWorld(hasHit);
	if (hasHit) {
		auto towerPlacement = Cast<ATowerPlacement>(hit.Actor.Get());
		if (towerPlacement == nullptr) {
			towerGhost->SetActorLocation(hit.ImpactPoint);
			canPlace = false;
		}
		else {
			towerGhost->SetActorLocation(towerPlacement->Arrow->GetComponentLocation());
			canPlace = true;
		}
	}
	
}

FHitResult AToweDefenserController::GetMousePostionFromWorld(bool& hasHit)
{
	const FRotator Rotation = GetPawn()->GetActorRotation();
	

	// get forward vector
	const FVector Direction = FRotationMatrix(Rotation).GetUnitAxis(EAxis::X);
	FVector position;
	FVector direction;
	DeprojectMousePositionToWorld(position, direction);
	FVector endLocation = position + (direction * 50000000000);

	TArray<AActor*> ignore;
	FHitResult result;


	hasHit = UKismetSystemLibrary::LineTraceSingle(this, position, endLocation, query, false, ignore, EDrawDebugTrace::ForDuration, result, true);

	return result;
}

void AToweDefenserController::StartPlacing(TSubclassOf<ATower> tower)
{
	ATower* towerPrefab = tower.GetDefaultObject();
	if (!CanSpendMoney(towerPrefab->cost)) {
		return;
	}

	towerGhost = GetWorld()->SpawnActor<ATowerGhost>(towerGhostClass, FVector::ZeroVector, FRotator::ZeroRotator);
	SetState(EPlayerState::PLACING);
	towerToSpawn = tower;
	towerGhost->StaticMeshTurretHead->SetStaticMesh(towerPrefab->StaticMeshTurretHead->GetStaticMesh());
	towerGhost->StaticMeshTurretBase->SetStaticMesh(towerPrefab->StaticMeshTurretBase->GetStaticMesh());
	
	
}

void AToweDefenserController::AddMoney(int32 value)
{
	OnAddMoney.Broadcast(value);
	currency += value;
}

void AToweDefenserController::SubtractMoney(int32 value)
{
	OnSubtractMoney.Broadcast(value);
	currency -= value;
}

void AToweDefenserController::SetState(EPlayerState nextState)
{
	switch (nextState) {
	case EPlayerState::PLACING:
		break;


	default:
		break;
	}
	state = nextState;
}

bool AToweDefenserController::CanSpendMoney(int32 value)
{
	return currency - value >= 0;
}

void AToweDefenserController::BeginPlay()
{
	Super::BeginPlay();
}

void AToweDefenserController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	HandlePlayerState();

}

void AToweDefenserController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Mouse0", IE_Pressed, this, &AToweDefenserController::PlayerClick);
}
