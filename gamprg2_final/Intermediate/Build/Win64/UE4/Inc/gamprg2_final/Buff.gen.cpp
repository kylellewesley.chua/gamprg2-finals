// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "gamprg2_final/Buff.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBuff() {}
// Cross Module References
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_UBuff_NoRegister();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_UBuff();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_gamprg2_final();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_UBuffReceiver_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UBuff::execTick)
	{
		P_GET_OBJECT(UBuffReceiver,Z_Param_buffreceiver);
		P_GET_PROPERTY(FFloatProperty,Z_Param_DeltaTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Tick_Implementation(Z_Param_buffreceiver,Z_Param_DeltaTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UBuff::execOnRemoveBuff)
	{
		P_GET_OBJECT(UBuffReceiver,Z_Param_buffreceiver);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnRemoveBuff_Implementation(Z_Param_buffreceiver);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UBuff::execOnReceiveBuff)
	{
		P_GET_OBJECT(UBuffReceiver,Z_Param_buffreceiver);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnReceiveBuff_Implementation(Z_Param_buffreceiver);
		P_NATIVE_END;
	}
	static FName NAME_UBuff_OnReceiveBuff = FName(TEXT("OnReceiveBuff"));
	void UBuff::OnReceiveBuff(UBuffReceiver* buffreceiver)
	{
		Buff_eventOnReceiveBuff_Parms Parms;
		Parms.buffreceiver=buffreceiver;
		ProcessEvent(FindFunctionChecked(NAME_UBuff_OnReceiveBuff),&Parms);
	}
	static FName NAME_UBuff_OnRemoveBuff = FName(TEXT("OnRemoveBuff"));
	void UBuff::OnRemoveBuff(UBuffReceiver* buffreceiver)
	{
		Buff_eventOnRemoveBuff_Parms Parms;
		Parms.buffreceiver=buffreceiver;
		ProcessEvent(FindFunctionChecked(NAME_UBuff_OnRemoveBuff),&Parms);
	}
	static FName NAME_UBuff_Tick = FName(TEXT("Tick"));
	void UBuff::Tick(UBuffReceiver* buffreceiver, float DeltaTime)
	{
		Buff_eventTick_Parms Parms;
		Parms.buffreceiver=buffreceiver;
		Parms.DeltaTime=DeltaTime;
		ProcessEvent(FindFunctionChecked(NAME_UBuff_Tick),&Parms);
	}
	void UBuff::StaticRegisterNativesUBuff()
	{
		UClass* Class = UBuff::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnReceiveBuff", &UBuff::execOnReceiveBuff },
			{ "OnRemoveBuff", &UBuff::execOnRemoveBuff },
			{ "Tick", &UBuff::execTick },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_buffreceiver_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_buffreceiver;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::NewProp_buffreceiver_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::NewProp_buffreceiver = { "buffreceiver", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Buff_eventOnReceiveBuff_Parms, buffreceiver), Z_Construct_UClass_UBuffReceiver_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::NewProp_buffreceiver_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::NewProp_buffreceiver_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::NewProp_buffreceiver,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Buff.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBuff, nullptr, "OnReceiveBuff", nullptr, nullptr, sizeof(Buff_eventOnReceiveBuff_Parms), Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBuff_OnReceiveBuff()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBuff_OnReceiveBuff_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_buffreceiver_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_buffreceiver;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::NewProp_buffreceiver_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::NewProp_buffreceiver = { "buffreceiver", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Buff_eventOnRemoveBuff_Parms, buffreceiver), Z_Construct_UClass_UBuffReceiver_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::NewProp_buffreceiver_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::NewProp_buffreceiver_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::NewProp_buffreceiver,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Buff.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBuff, nullptr, "OnRemoveBuff", nullptr, nullptr, sizeof(Buff_eventOnRemoveBuff_Parms), Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBuff_OnRemoveBuff()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBuff_OnRemoveBuff_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UBuff_Tick_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_buffreceiver_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_buffreceiver;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UBuff_Tick_Statics::NewProp_DeltaTime = { "DeltaTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Buff_eventTick_Parms, DeltaTime), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuff_Tick_Statics::NewProp_buffreceiver_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UBuff_Tick_Statics::NewProp_buffreceiver = { "buffreceiver", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Buff_eventTick_Parms, buffreceiver), Z_Construct_UClass_UBuffReceiver_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UBuff_Tick_Statics::NewProp_buffreceiver_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuff_Tick_Statics::NewProp_buffreceiver_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBuff_Tick_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuff_Tick_Statics::NewProp_DeltaTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuff_Tick_Statics::NewProp_buffreceiver,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuff_Tick_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Buff.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBuff_Tick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBuff, nullptr, "Tick", nullptr, nullptr, sizeof(Buff_eventTick_Parms), Z_Construct_UFunction_UBuff_Tick_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuff_Tick_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBuff_Tick_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuff_Tick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBuff_Tick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBuff_Tick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UBuff_NoRegister()
	{
		return UBuff::StaticClass();
	}
	struct Z_Construct_UClass_UBuff_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_shouldTick_MetaData[];
#endif
		static void NewProp_shouldTick_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_shouldTick;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBuff_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_gamprg2_final,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UBuff_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UBuff_OnReceiveBuff, "OnReceiveBuff" }, // 3977855482
		{ &Z_Construct_UFunction_UBuff_OnRemoveBuff, "OnRemoveBuff" }, // 955090695
		{ &Z_Construct_UFunction_UBuff_Tick, "Tick" }, // 2125026360
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuff_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Buff.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Buff.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuff_Statics::NewProp_shouldTick_MetaData[] = {
		{ "Category", "Buff" },
		{ "ModuleRelativePath", "Buff.h" },
	};
#endif
	void Z_Construct_UClass_UBuff_Statics::NewProp_shouldTick_SetBit(void* Obj)
	{
		((UBuff*)Obj)->shouldTick = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UBuff_Statics::NewProp_shouldTick = { "shouldTick", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UBuff), &Z_Construct_UClass_UBuff_Statics::NewProp_shouldTick_SetBit, METADATA_PARAMS(Z_Construct_UClass_UBuff_Statics::NewProp_shouldTick_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBuff_Statics::NewProp_shouldTick_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBuff_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuff_Statics::NewProp_shouldTick,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBuff_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBuff>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBuff_Statics::ClassParams = {
		&UBuff::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UBuff_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UBuff_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBuff_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBuff_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBuff()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBuff_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBuff, 2678767556);
	template<> GAMPRG2_FINAL_API UClass* StaticClass<UBuff>()
	{
		return UBuff::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBuff(Z_Construct_UClass_UBuff, &UBuff::StaticClass, TEXT("/Script/gamprg2_final"), TEXT("UBuff"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBuff);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
