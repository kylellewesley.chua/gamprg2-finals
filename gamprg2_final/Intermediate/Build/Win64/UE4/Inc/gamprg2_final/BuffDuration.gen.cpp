// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "gamprg2_final/BuffDuration.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBuffDuration() {}
// Cross Module References
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_UBuffDuration_NoRegister();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_UBuffDuration();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_UBuff();
	UPackage* Z_Construct_UPackage__Script_gamprg2_final();
// End Cross Module References
	DEFINE_FUNCTION(UBuffDuration::execResetDuration)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetDuration();
		P_NATIVE_END;
	}
	void UBuffDuration::StaticRegisterNativesUBuffDuration()
	{
		UClass* Class = UBuffDuration::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ResetDuration", &UBuffDuration::execResetDuration },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UBuffDuration_ResetDuration_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuffDuration_ResetDuration_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BuffDuration.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBuffDuration_ResetDuration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBuffDuration, nullptr, "ResetDuration", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBuffDuration_ResetDuration_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuffDuration_ResetDuration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBuffDuration_ResetDuration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBuffDuration_ResetDuration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UBuffDuration_NoRegister()
	{
		return UBuffDuration::StaticClass();
	}
	struct Z_Construct_UClass_UBuffDuration_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_currentDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_currentDuration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_duration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_duration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBuffDuration_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBuff,
		(UObject* (*)())Z_Construct_UPackage__Script_gamprg2_final,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UBuffDuration_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UBuffDuration_ResetDuration, "ResetDuration" }, // 3867127132
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuffDuration_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "BuffDuration.h" },
		{ "ModuleRelativePath", "BuffDuration.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuffDuration_Statics::NewProp_currentDuration_MetaData[] = {
		{ "Category", "BuffDuration" },
		{ "ModuleRelativePath", "BuffDuration.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBuffDuration_Statics::NewProp_currentDuration = { "currentDuration", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBuffDuration, currentDuration), METADATA_PARAMS(Z_Construct_UClass_UBuffDuration_Statics::NewProp_currentDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBuffDuration_Statics::NewProp_currentDuration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuffDuration_Statics::NewProp_duration_MetaData[] = {
		{ "Category", "BuffDuration" },
		{ "ModuleRelativePath", "BuffDuration.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UBuffDuration_Statics::NewProp_duration = { "duration", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBuffDuration, duration), METADATA_PARAMS(Z_Construct_UClass_UBuffDuration_Statics::NewProp_duration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBuffDuration_Statics::NewProp_duration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBuffDuration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuffDuration_Statics::NewProp_currentDuration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuffDuration_Statics::NewProp_duration,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBuffDuration_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBuffDuration>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBuffDuration_Statics::ClassParams = {
		&UBuffDuration::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UBuffDuration_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UBuffDuration_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBuffDuration_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBuffDuration_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBuffDuration()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBuffDuration_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBuffDuration, 645258611);
	template<> GAMPRG2_FINAL_API UClass* StaticClass<UBuffDuration>()
	{
		return UBuffDuration::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBuffDuration(Z_Construct_UClass_UBuffDuration, &UBuffDuration::StaticClass, TEXT("/Script/gamprg2_final"), TEXT("UBuffDuration"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBuffDuration);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
