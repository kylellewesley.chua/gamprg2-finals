// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "gamprg2_final/EnemyTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnemyTypes() {}
// Cross Module References
	GAMPRG2_FINAL_API UEnum* Z_Construct_UEnum_gamprg2_final_EEnemyGait();
	UPackage* Z_Construct_UPackage__Script_gamprg2_final();
	GAMPRG2_FINAL_API UEnum* Z_Construct_UEnum_gamprg2_final_EEnemyTypes();
// End Cross Module References
	static UEnum* EEnemyGait_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_gamprg2_final_EEnemyGait, Z_Construct_UPackage__Script_gamprg2_final(), TEXT("EEnemyGait"));
		}
		return Singleton;
	}
	template<> GAMPRG2_FINAL_API UEnum* StaticEnum<EEnemyGait>()
	{
		return EEnemyGait_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EEnemyGait(EEnemyGait_StaticEnum, TEXT("/Script/gamprg2_final"), TEXT("EEnemyGait"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_gamprg2_final_EEnemyGait_Hash() { return 2258696059U; }
	UEnum* Z_Construct_UEnum_gamprg2_final_EEnemyGait()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_gamprg2_final();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EEnemyGait"), 0, Get_Z_Construct_UEnum_gamprg2_final_EEnemyGait_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EEnemyGait::Grounded", (int64)EEnemyGait::Grounded },
				{ "EEnemyGait::Flying", (int64)EEnemyGait::Flying },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Flying.Name", "EEnemyGait::Flying" },
				{ "Grounded.Name", "EEnemyGait::Grounded" },
				{ "ModuleRelativePath", "EnemyTypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_gamprg2_final,
				nullptr,
				"EEnemyGait",
				"EEnemyGait",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EEnemyTypes_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_gamprg2_final_EEnemyTypes, Z_Construct_UPackage__Script_gamprg2_final(), TEXT("EEnemyTypes"));
		}
		return Singleton;
	}
	template<> GAMPRG2_FINAL_API UEnum* StaticEnum<EEnemyTypes>()
	{
		return EEnemyTypes_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EEnemyTypes(EEnemyTypes_StaticEnum, TEXT("/Script/gamprg2_final"), TEXT("EEnemyTypes"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_gamprg2_final_EEnemyTypes_Hash() { return 2616989584U; }
	UEnum* Z_Construct_UEnum_gamprg2_final_EEnemyTypes()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_gamprg2_final();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EEnemyTypes"), 0, Get_Z_Construct_UEnum_gamprg2_final_EEnemyTypes_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EEnemyTypes::Normal", (int64)EEnemyTypes::Normal },
				{ "EEnemyTypes::Boss", (int64)EEnemyTypes::Boss },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Boss.Comment", "/**\n * \n */" },
				{ "Boss.Name", "EEnemyTypes::Boss" },
				{ "Comment", "/**\n * \n */" },
				{ "ModuleRelativePath", "EnemyTypes.h" },
				{ "Normal.Comment", "/**\n * \n */" },
				{ "Normal.Name", "EEnemyTypes::Normal" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_gamprg2_final,
				nullptr,
				"EEnemyTypes",
				"EEnemyTypes",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
