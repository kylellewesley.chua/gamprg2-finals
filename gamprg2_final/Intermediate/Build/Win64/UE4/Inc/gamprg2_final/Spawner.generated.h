// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRG2_FINAL_Spawner_generated_h
#error "Spawner.generated.h already included, missing '#pragma once' in Spawner.h"
#endif
#define GAMPRG2_FINAL_Spawner_generated_h

#define gamprg2_final_Source_gamprg2_final_Spawner_h_11_DELEGATE \
static inline void FOnClearWave_DelegateWrapper(const FMulticastScriptDelegate& OnClearWave) \
{ \
	OnClearWave.ProcessMulticastDelegate<UObject>(NULL); \
}


#define gamprg2_final_Source_gamprg2_final_Spawner_h_17_SPARSE_DATA
#define gamprg2_final_Source_gamprg2_final_Spawner_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execEnemyDeaths); \
	DECLARE_FUNCTION(execSpawn); \
	DECLARE_FUNCTION(execInitializeSpawn);


#define gamprg2_final_Source_gamprg2_final_Spawner_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execEnemyDeaths); \
	DECLARE_FUNCTION(execSpawn); \
	DECLARE_FUNCTION(execInitializeSpawn);


#define gamprg2_final_Source_gamprg2_final_Spawner_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawner(); \
	friend struct Z_Construct_UClass_ASpawner_Statics; \
public: \
	DECLARE_CLASS(ASpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(ASpawner)


#define gamprg2_final_Source_gamprg2_final_Spawner_h_17_INCLASS \
private: \
	static void StaticRegisterNativesASpawner(); \
	friend struct Z_Construct_UClass_ASpawner_Statics; \
public: \
	DECLARE_CLASS(ASpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(ASpawner)


#define gamprg2_final_Source_gamprg2_final_Spawner_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawner(ASpawner&&); \
	NO_API ASpawner(const ASpawner&); \
public:


#define gamprg2_final_Source_gamprg2_final_Spawner_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawner(ASpawner&&); \
	NO_API ASpawner(const ASpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawner)


#define gamprg2_final_Source_gamprg2_final_Spawner_h_17_PRIVATE_PROPERTY_OFFSET
#define gamprg2_final_Source_gamprg2_final_Spawner_h_14_PROLOG
#define gamprg2_final_Source_gamprg2_final_Spawner_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_Spawner_h_17_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_Spawner_h_17_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_Spawner_h_17_RPC_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_Spawner_h_17_INCLASS \
	gamprg2_final_Source_gamprg2_final_Spawner_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gamprg2_final_Source_gamprg2_final_Spawner_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_Spawner_h_17_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_Spawner_h_17_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_Spawner_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_Spawner_h_17_INCLASS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_Spawner_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPRG2_FINAL_API UClass* StaticClass<class ASpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_Spawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
