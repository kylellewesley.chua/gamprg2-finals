// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "gamprg2_final/BuffReceiver.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBuffReceiver() {}
// Cross Module References
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_UBuffReceiver_NoRegister();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_UBuffReceiver();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_gamprg2_final();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_UBuff_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UBuffReceiver::execGetBuff)
	{
		P_GET_OBJECT(UClass,Z_Param_buff);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UBuff**)Z_Param__Result=P_THIS->GetBuff(Z_Param_buff);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UBuffReceiver::execContainsBuff)
	{
		P_GET_OBJECT(UClass,Z_Param_buff);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ContainsBuff(Z_Param_buff);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UBuffReceiver::execRemoveBuff)
	{
		P_GET_OBJECT(UClass,Z_Param_buff);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveBuff(Z_Param_buff);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UBuffReceiver::execAddBuff)
	{
		P_GET_OBJECT(UClass,Z_Param_buff);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddBuff(Z_Param_buff);
		P_NATIVE_END;
	}
	void UBuffReceiver::StaticRegisterNativesUBuffReceiver()
	{
		UClass* Class = UBuffReceiver::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddBuff", &UBuffReceiver::execAddBuff },
			{ "ContainsBuff", &UBuffReceiver::execContainsBuff },
			{ "GetBuff", &UBuffReceiver::execGetBuff },
			{ "RemoveBuff", &UBuffReceiver::execRemoveBuff },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UBuffReceiver_AddBuff_Statics
	{
		struct BuffReceiver_eventAddBuff_Parms
		{
			TSubclassOf<UBuff>  buff;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_buff;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UBuffReceiver_AddBuff_Statics::NewProp_buff = { "buff", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuffReceiver_eventAddBuff_Parms, buff), Z_Construct_UClass_UBuff_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBuffReceiver_AddBuff_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuffReceiver_AddBuff_Statics::NewProp_buff,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuffReceiver_AddBuff_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BuffReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBuffReceiver_AddBuff_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBuffReceiver, nullptr, "AddBuff", nullptr, nullptr, sizeof(BuffReceiver_eventAddBuff_Parms), Z_Construct_UFunction_UBuffReceiver_AddBuff_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuffReceiver_AddBuff_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBuffReceiver_AddBuff_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuffReceiver_AddBuff_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBuffReceiver_AddBuff()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBuffReceiver_AddBuff_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics
	{
		struct BuffReceiver_eventContainsBuff_Parms
		{
			TSubclassOf<UBuff>  buff;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_buff;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((BuffReceiver_eventContainsBuff_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(BuffReceiver_eventContainsBuff_Parms), &Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::NewProp_buff = { "buff", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuffReceiver_eventContainsBuff_Parms, buff), Z_Construct_UClass_UBuff_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::NewProp_buff,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BuffReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBuffReceiver, nullptr, "ContainsBuff", nullptr, nullptr, sizeof(BuffReceiver_eventContainsBuff_Parms), Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBuffReceiver_ContainsBuff()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBuffReceiver_ContainsBuff_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics
	{
		struct BuffReceiver_eventGetBuff_Parms
		{
			TSubclassOf<UBuff>  buff;
			UBuff* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_buff;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuffReceiver_eventGetBuff_Parms, ReturnValue), Z_Construct_UClass_UBuff_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics::NewProp_buff = { "buff", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuffReceiver_eventGetBuff_Parms, buff), Z_Construct_UClass_UBuff_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics::NewProp_buff,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BuffReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBuffReceiver, nullptr, "GetBuff", nullptr, nullptr, sizeof(BuffReceiver_eventGetBuff_Parms), Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBuffReceiver_GetBuff()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBuffReceiver_GetBuff_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UBuffReceiver_RemoveBuff_Statics
	{
		struct BuffReceiver_eventRemoveBuff_Parms
		{
			TSubclassOf<UBuff>  buff;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_buff;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UBuffReceiver_RemoveBuff_Statics::NewProp_buff = { "buff", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuffReceiver_eventRemoveBuff_Parms, buff), Z_Construct_UClass_UBuff_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBuffReceiver_RemoveBuff_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuffReceiver_RemoveBuff_Statics::NewProp_buff,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuffReceiver_RemoveBuff_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BuffReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBuffReceiver_RemoveBuff_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBuffReceiver, nullptr, "RemoveBuff", nullptr, nullptr, sizeof(BuffReceiver_eventRemoveBuff_Parms), Z_Construct_UFunction_UBuffReceiver_RemoveBuff_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuffReceiver_RemoveBuff_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBuffReceiver_RemoveBuff_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuffReceiver_RemoveBuff_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBuffReceiver_RemoveBuff()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBuffReceiver_RemoveBuff_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UBuffReceiver_NoRegister()
	{
		return UBuffReceiver::StaticClass();
	}
	struct Z_Construct_UClass_UBuffReceiver_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_buffList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_buffList;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_buffList_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBuffReceiver_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_gamprg2_final,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UBuffReceiver_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UBuffReceiver_AddBuff, "AddBuff" }, // 1123187090
		{ &Z_Construct_UFunction_UBuffReceiver_ContainsBuff, "ContainsBuff" }, // 83219032
		{ &Z_Construct_UFunction_UBuffReceiver_GetBuff, "GetBuff" }, // 3767945339
		{ &Z_Construct_UFunction_UBuffReceiver_RemoveBuff, "RemoveBuff" }, // 1888742855
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuffReceiver_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "BuffReceiver.h" },
		{ "ModuleRelativePath", "BuffReceiver.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuffReceiver_Statics::NewProp_buffList_MetaData[] = {
		{ "Category", "BuffReceiver" },
		{ "ModuleRelativePath", "BuffReceiver.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UBuffReceiver_Statics::NewProp_buffList = { "buffList", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBuffReceiver, buffList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UBuffReceiver_Statics::NewProp_buffList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBuffReceiver_Statics::NewProp_buffList_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBuffReceiver_Statics::NewProp_buffList_Inner = { "buffList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UBuff_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBuffReceiver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuffReceiver_Statics::NewProp_buffList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuffReceiver_Statics::NewProp_buffList_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBuffReceiver_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBuffReceiver>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBuffReceiver_Statics::ClassParams = {
		&UBuffReceiver::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UBuffReceiver_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UBuffReceiver_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UBuffReceiver_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBuffReceiver_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBuffReceiver()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBuffReceiver_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBuffReceiver, 3436770954);
	template<> GAMPRG2_FINAL_API UClass* StaticClass<UBuffReceiver>()
	{
		return UBuffReceiver::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBuffReceiver(Z_Construct_UClass_UBuffReceiver, &UBuffReceiver::StaticClass, TEXT("/Script/gamprg2_final"), TEXT("UBuffReceiver"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBuffReceiver);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
