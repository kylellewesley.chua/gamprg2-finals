// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EPlayerState : uint8;
class ATower;
struct FHitResult;
#ifdef GAMPRG2_FINAL_ToweDefenserController_generated_h
#error "ToweDefenserController.generated.h already included, missing '#pragma once' in ToweDefenserController.h"
#endif
#define GAMPRG2_FINAL_ToweDefenserController_generated_h

#define gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_14_DELEGATE \
struct _Script_gamprg2_final_eventMoneySignature_Parms \
{ \
	int32 value; \
}; \
static inline void FMoneySignature_DelegateWrapper(const FMulticastScriptDelegate& MoneySignature, int32 value) \
{ \
	_Script_gamprg2_final_eventMoneySignature_Parms Parms; \
	Parms.value=value; \
	MoneySignature.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_SPARSE_DATA
#define gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCurrency); \
	DECLARE_FUNCTION(execCanSpendMoney); \
	DECLARE_FUNCTION(execSetState); \
	DECLARE_FUNCTION(execSubtractMoney); \
	DECLARE_FUNCTION(execAddMoney); \
	DECLARE_FUNCTION(execStartPlacing); \
	DECLARE_FUNCTION(execGetMousePostionFromWorld); \
	DECLARE_FUNCTION(execUpdateTowerGhost); \
	DECLARE_FUNCTION(execPlaceTower); \
	DECLARE_FUNCTION(execHandlePlayerState); \
	DECLARE_FUNCTION(execPlayerClick);


#define gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCurrency); \
	DECLARE_FUNCTION(execCanSpendMoney); \
	DECLARE_FUNCTION(execSetState); \
	DECLARE_FUNCTION(execSubtractMoney); \
	DECLARE_FUNCTION(execAddMoney); \
	DECLARE_FUNCTION(execStartPlacing); \
	DECLARE_FUNCTION(execGetMousePostionFromWorld); \
	DECLARE_FUNCTION(execUpdateTowerGhost); \
	DECLARE_FUNCTION(execPlaceTower); \
	DECLARE_FUNCTION(execHandlePlayerState); \
	DECLARE_FUNCTION(execPlayerClick);


#define gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAToweDefenserController(); \
	friend struct Z_Construct_UClass_AToweDefenserController_Statics; \
public: \
	DECLARE_CLASS(AToweDefenserController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(AToweDefenserController)


#define gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_INCLASS \
private: \
	static void StaticRegisterNativesAToweDefenserController(); \
	friend struct Z_Construct_UClass_AToweDefenserController_Statics; \
public: \
	DECLARE_CLASS(AToweDefenserController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(AToweDefenserController)


#define gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AToweDefenserController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AToweDefenserController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AToweDefenserController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AToweDefenserController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AToweDefenserController(AToweDefenserController&&); \
	NO_API AToweDefenserController(const AToweDefenserController&); \
public:


#define gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AToweDefenserController(AToweDefenserController&&); \
	NO_API AToweDefenserController(const AToweDefenserController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AToweDefenserController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AToweDefenserController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AToweDefenserController)


#define gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_PRIVATE_PROPERTY_OFFSET
#define gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_28_PROLOG
#define gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_RPC_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_INCLASS \
	gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_INCLASS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_ToweDefenserController_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPRG2_FINAL_API UClass* StaticClass<class AToweDefenserController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_ToweDefenserController_h


#define FOREACH_ENUM_EPLAYERSTATE(op) \
	op(EPlayerState::SELECTING) \
	op(EPlayerState::PLACING) \
	op(EPlayerState::UPGRADING) 

enum class EPlayerState : uint8;
template<> GAMPRG2_FINAL_API UEnum* StaticEnum<EPlayerState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
