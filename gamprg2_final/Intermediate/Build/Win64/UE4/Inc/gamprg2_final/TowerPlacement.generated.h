// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRG2_FINAL_TowerPlacement_generated_h
#error "TowerPlacement.generated.h already included, missing '#pragma once' in TowerPlacement.h"
#endif
#define GAMPRG2_FINAL_TowerPlacement_generated_h

#define gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_SPARSE_DATA
#define gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_RPC_WRAPPERS
#define gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATowerPlacement(); \
	friend struct Z_Construct_UClass_ATowerPlacement_Statics; \
public: \
	DECLARE_CLASS(ATowerPlacement, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(ATowerPlacement)


#define gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATowerPlacement(); \
	friend struct Z_Construct_UClass_ATowerPlacement_Statics; \
public: \
	DECLARE_CLASS(ATowerPlacement, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(ATowerPlacement)


#define gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATowerPlacement(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATowerPlacement) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerPlacement); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerPlacement); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerPlacement(ATowerPlacement&&); \
	NO_API ATowerPlacement(const ATowerPlacement&); \
public:


#define gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerPlacement(ATowerPlacement&&); \
	NO_API ATowerPlacement(const ATowerPlacement&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerPlacement); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerPlacement); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATowerPlacement)


#define gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_PRIVATE_PROPERTY_OFFSET
#define gamprg2_final_Source_gamprg2_final_TowerPlacement_h_12_PROLOG
#define gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_RPC_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_INCLASS \
	gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_INCLASS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_TowerPlacement_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPRG2_FINAL_API UClass* StaticClass<class ATowerPlacement>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_TowerPlacement_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
