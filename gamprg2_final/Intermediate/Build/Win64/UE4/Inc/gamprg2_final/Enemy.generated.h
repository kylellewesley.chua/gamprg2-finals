// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRG2_FINAL_Enemy_generated_h
#error "Enemy.generated.h already included, missing '#pragma once' in Enemy.h"
#endif
#define GAMPRG2_FINAL_Enemy_generated_h

#define gamprg2_final_Source_gamprg2_final_Enemy_h_15_DELEGATE \
static inline void FOnDeath_DelegateWrapper(const FMulticastScriptDelegate& OnDeath) \
{ \
	OnDeath.ProcessMulticastDelegate<UObject>(NULL); \
}


#define gamprg2_final_Source_gamprg2_final_Enemy_h_20_SPARSE_DATA
#define gamprg2_final_Source_gamprg2_final_Enemy_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execReachPlayerCore); \
	DECLARE_FUNCTION(execGiveMoneyToPlayer);


#define gamprg2_final_Source_gamprg2_final_Enemy_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execReachPlayerCore); \
	DECLARE_FUNCTION(execGiveMoneyToPlayer);


#define gamprg2_final_Source_gamprg2_final_Enemy_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemy(); \
	friend struct Z_Construct_UClass_AEnemy_Statics; \
public: \
	DECLARE_CLASS(AEnemy, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(AEnemy)


#define gamprg2_final_Source_gamprg2_final_Enemy_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAEnemy(); \
	friend struct Z_Construct_UClass_AEnemy_Statics; \
public: \
	DECLARE_CLASS(AEnemy, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(AEnemy)


#define gamprg2_final_Source_gamprg2_final_Enemy_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemy(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemy(AEnemy&&); \
	NO_API AEnemy(const AEnemy&); \
public:


#define gamprg2_final_Source_gamprg2_final_Enemy_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemy(AEnemy&&); \
	NO_API AEnemy(const AEnemy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemy); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEnemy)


#define gamprg2_final_Source_gamprg2_final_Enemy_h_20_PRIVATE_PROPERTY_OFFSET
#define gamprg2_final_Source_gamprg2_final_Enemy_h_17_PROLOG
#define gamprg2_final_Source_gamprg2_final_Enemy_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_Enemy_h_20_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_Enemy_h_20_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_Enemy_h_20_RPC_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_Enemy_h_20_INCLASS \
	gamprg2_final_Source_gamprg2_final_Enemy_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gamprg2_final_Source_gamprg2_final_Enemy_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_Enemy_h_20_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_Enemy_h_20_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_Enemy_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_Enemy_h_20_INCLASS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_Enemy_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPRG2_FINAL_API UClass* StaticClass<class AEnemy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_Enemy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
