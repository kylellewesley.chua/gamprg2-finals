// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "gamprg2_final/ToweDefenserController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeToweDefenserController() {}
// Cross Module References
	GAMPRG2_FINAL_API UFunction* Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_gamprg2_final();
	GAMPRG2_FINAL_API UEnum* Z_Construct_UEnum_gamprg2_final_EPlayerState();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_AToweDefenserController_NoRegister();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_AToweDefenserController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_ATower_NoRegister();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_ATowerGhost_NoRegister();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_UHealthComponent_NoRegister();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ETraceTypeQuery();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature_Statics
	{
		struct _Script_gamprg2_final_eventMoneySignature_Parms
		{
			int32 value;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature_Statics::NewProp_value = { "value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_gamprg2_final_eventMoneySignature_Parms, value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature_Statics::NewProp_value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_gamprg2_final, nullptr, "MoneySignature__DelegateSignature", nullptr, nullptr, sizeof(_Script_gamprg2_final_eventMoneySignature_Parms), Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EPlayerState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_gamprg2_final_EPlayerState, Z_Construct_UPackage__Script_gamprg2_final(), TEXT("EPlayerState"));
		}
		return Singleton;
	}
	template<> GAMPRG2_FINAL_API UEnum* StaticEnum<EPlayerState>()
	{
		return EPlayerState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPlayerState(EPlayerState_StaticEnum, TEXT("/Script/gamprg2_final"), TEXT("EPlayerState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_gamprg2_final_EPlayerState_Hash() { return 1276863953U; }
	UEnum* Z_Construct_UEnum_gamprg2_final_EPlayerState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_gamprg2_final();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPlayerState"), 0, Get_Z_Construct_UEnum_gamprg2_final_EPlayerState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPlayerState::SELECTING", (int64)EPlayerState::SELECTING },
				{ "EPlayerState::PLACING", (int64)EPlayerState::PLACING },
				{ "EPlayerState::UPGRADING", (int64)EPlayerState::UPGRADING },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "ToweDefenserController.h" },
				{ "PLACING.Name", "EPlayerState::PLACING" },
				{ "SELECTING.Name", "EPlayerState::SELECTING" },
				{ "UPGRADING.Name", "EPlayerState::UPGRADING" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_gamprg2_final,
				nullptr,
				"EPlayerState",
				"EPlayerState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(AToweDefenserController::execGetCurrency)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetCurrency();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AToweDefenserController::execCanSpendMoney)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_value);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CanSpendMoney(Z_Param_value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AToweDefenserController::execSetState)
	{
		P_GET_ENUM(EPlayerState,Z_Param_nextState);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetState(EPlayerState(Z_Param_nextState));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AToweDefenserController::execSubtractMoney)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SubtractMoney(Z_Param_value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AToweDefenserController::execAddMoney)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddMoney(Z_Param_value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AToweDefenserController::execStartPlacing)
	{
		P_GET_OBJECT(UClass,Z_Param_tower);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartPlacing(Z_Param_tower);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AToweDefenserController::execGetMousePostionFromWorld)
	{
		P_GET_UBOOL_REF(Z_Param_Out_hasHit);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FHitResult*)Z_Param__Result=P_THIS->GetMousePostionFromWorld(Z_Param_Out_hasHit);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AToweDefenserController::execUpdateTowerGhost)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UpdateTowerGhost();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AToweDefenserController::execPlaceTower)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->PlaceTower();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AToweDefenserController::execHandlePlayerState)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandlePlayerState();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AToweDefenserController::execPlayerClick)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->PlayerClick();
		P_NATIVE_END;
	}
	void AToweDefenserController::StaticRegisterNativesAToweDefenserController()
	{
		UClass* Class = AToweDefenserController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddMoney", &AToweDefenserController::execAddMoney },
			{ "CanSpendMoney", &AToweDefenserController::execCanSpendMoney },
			{ "GetCurrency", &AToweDefenserController::execGetCurrency },
			{ "GetMousePostionFromWorld", &AToweDefenserController::execGetMousePostionFromWorld },
			{ "HandlePlayerState", &AToweDefenserController::execHandlePlayerState },
			{ "PlaceTower", &AToweDefenserController::execPlaceTower },
			{ "PlayerClick", &AToweDefenserController::execPlayerClick },
			{ "SetState", &AToweDefenserController::execSetState },
			{ "StartPlacing", &AToweDefenserController::execStartPlacing },
			{ "SubtractMoney", &AToweDefenserController::execSubtractMoney },
			{ "UpdateTowerGhost", &AToweDefenserController::execUpdateTowerGhost },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AToweDefenserController_AddMoney_Statics
	{
		struct ToweDefenserController_eventAddMoney_Parms
		{
			int32 value;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AToweDefenserController_AddMoney_Statics::NewProp_value = { "value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ToweDefenserController_eventAddMoney_Parms, value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AToweDefenserController_AddMoney_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AToweDefenserController_AddMoney_Statics::NewProp_value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AToweDefenserController_AddMoney_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AToweDefenserController_AddMoney_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AToweDefenserController, nullptr, "AddMoney", nullptr, nullptr, sizeof(ToweDefenserController_eventAddMoney_Parms), Z_Construct_UFunction_AToweDefenserController_AddMoney_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_AddMoney_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AToweDefenserController_AddMoney_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_AddMoney_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AToweDefenserController_AddMoney()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AToweDefenserController_AddMoney_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics
	{
		struct ToweDefenserController_eventCanSpendMoney_Parms
		{
			int32 value;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ToweDefenserController_eventCanSpendMoney_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ToweDefenserController_eventCanSpendMoney_Parms), &Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::NewProp_value = { "value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ToweDefenserController_eventCanSpendMoney_Parms, value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::NewProp_value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AToweDefenserController, nullptr, "CanSpendMoney", nullptr, nullptr, sizeof(ToweDefenserController_eventCanSpendMoney_Parms), Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AToweDefenserController_CanSpendMoney()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AToweDefenserController_CanSpendMoney_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AToweDefenserController_GetCurrency_Statics
	{
		struct ToweDefenserController_eventGetCurrency_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AToweDefenserController_GetCurrency_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ToweDefenserController_eventGetCurrency_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AToweDefenserController_GetCurrency_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AToweDefenserController_GetCurrency_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AToweDefenserController_GetCurrency_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AToweDefenserController_GetCurrency_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AToweDefenserController, nullptr, "GetCurrency", nullptr, nullptr, sizeof(ToweDefenserController_eventGetCurrency_Parms), Z_Construct_UFunction_AToweDefenserController_GetCurrency_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_GetCurrency_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AToweDefenserController_GetCurrency_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_GetCurrency_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AToweDefenserController_GetCurrency()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AToweDefenserController_GetCurrency_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics
	{
		struct ToweDefenserController_eventGetMousePostionFromWorld_Parms
		{
			bool hasHit;
			FHitResult ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static void NewProp_hasHit_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_hasHit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ToweDefenserController_eventGetMousePostionFromWorld_Parms, ReturnValue), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::NewProp_hasHit_SetBit(void* Obj)
	{
		((ToweDefenserController_eventGetMousePostionFromWorld_Parms*)Obj)->hasHit = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::NewProp_hasHit = { "hasHit", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ToweDefenserController_eventGetMousePostionFromWorld_Parms), &Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::NewProp_hasHit_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::NewProp_hasHit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AToweDefenserController, nullptr, "GetMousePostionFromWorld", nullptr, nullptr, sizeof(ToweDefenserController_eventGetMousePostionFromWorld_Parms), Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AToweDefenserController_HandlePlayerState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AToweDefenserController_HandlePlayerState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AToweDefenserController_HandlePlayerState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AToweDefenserController, nullptr, "HandlePlayerState", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AToweDefenserController_HandlePlayerState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_HandlePlayerState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AToweDefenserController_HandlePlayerState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AToweDefenserController_HandlePlayerState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AToweDefenserController_PlaceTower_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AToweDefenserController_PlaceTower_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AToweDefenserController_PlaceTower_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AToweDefenserController, nullptr, "PlaceTower", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AToweDefenserController_PlaceTower_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_PlaceTower_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AToweDefenserController_PlaceTower()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AToweDefenserController_PlaceTower_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AToweDefenserController_PlayerClick_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AToweDefenserController_PlayerClick_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AToweDefenserController_PlayerClick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AToweDefenserController, nullptr, "PlayerClick", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AToweDefenserController_PlayerClick_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_PlayerClick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AToweDefenserController_PlayerClick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AToweDefenserController_PlayerClick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AToweDefenserController_SetState_Statics
	{
		struct ToweDefenserController_eventSetState_Parms
		{
			EPlayerState nextState;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_nextState;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_nextState_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_AToweDefenserController_SetState_Statics::NewProp_nextState = { "nextState", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ToweDefenserController_eventSetState_Parms, nextState), Z_Construct_UEnum_gamprg2_final_EPlayerState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_AToweDefenserController_SetState_Statics::NewProp_nextState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AToweDefenserController_SetState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AToweDefenserController_SetState_Statics::NewProp_nextState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AToweDefenserController_SetState_Statics::NewProp_nextState_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AToweDefenserController_SetState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AToweDefenserController_SetState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AToweDefenserController, nullptr, "SetState", nullptr, nullptr, sizeof(ToweDefenserController_eventSetState_Parms), Z_Construct_UFunction_AToweDefenserController_SetState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_SetState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AToweDefenserController_SetState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_SetState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AToweDefenserController_SetState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AToweDefenserController_SetState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AToweDefenserController_StartPlacing_Statics
	{
		struct ToweDefenserController_eventStartPlacing_Parms
		{
			TSubclassOf<ATower>  tower;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_tower;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_AToweDefenserController_StartPlacing_Statics::NewProp_tower = { "tower", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ToweDefenserController_eventStartPlacing_Parms, tower), Z_Construct_UClass_ATower_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AToweDefenserController_StartPlacing_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AToweDefenserController_StartPlacing_Statics::NewProp_tower,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AToweDefenserController_StartPlacing_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AToweDefenserController_StartPlacing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AToweDefenserController, nullptr, "StartPlacing", nullptr, nullptr, sizeof(ToweDefenserController_eventStartPlacing_Parms), Z_Construct_UFunction_AToweDefenserController_StartPlacing_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_StartPlacing_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AToweDefenserController_StartPlacing_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_StartPlacing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AToweDefenserController_StartPlacing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AToweDefenserController_StartPlacing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AToweDefenserController_SubtractMoney_Statics
	{
		struct ToweDefenserController_eventSubtractMoney_Parms
		{
			int32 value;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AToweDefenserController_SubtractMoney_Statics::NewProp_value = { "value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ToweDefenserController_eventSubtractMoney_Parms, value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AToweDefenserController_SubtractMoney_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AToweDefenserController_SubtractMoney_Statics::NewProp_value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AToweDefenserController_SubtractMoney_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AToweDefenserController_SubtractMoney_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AToweDefenserController, nullptr, "SubtractMoney", nullptr, nullptr, sizeof(ToweDefenserController_eventSubtractMoney_Parms), Z_Construct_UFunction_AToweDefenserController_SubtractMoney_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_SubtractMoney_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AToweDefenserController_SubtractMoney_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_SubtractMoney_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AToweDefenserController_SubtractMoney()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AToweDefenserController_SubtractMoney_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AToweDefenserController_UpdateTowerGhost_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AToweDefenserController_UpdateTowerGhost_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AToweDefenserController_UpdateTowerGhost_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AToweDefenserController, nullptr, "UpdateTowerGhost", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AToweDefenserController_UpdateTowerGhost_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AToweDefenserController_UpdateTowerGhost_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AToweDefenserController_UpdateTowerGhost()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AToweDefenserController_UpdateTowerGhost_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AToweDefenserController_NoRegister()
	{
		return AToweDefenserController::StaticClass();
	}
	struct Z_Construct_UClass_AToweDefenserController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSubtractMoney_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSubtractMoney;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnAddMoney_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnAddMoney;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_towerGhostClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_towerGhostClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HealthComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HealthComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_currency_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_currency;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_towerToSpawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_towerToSpawn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_query_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_query;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AToweDefenserController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_gamprg2_final,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AToweDefenserController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AToweDefenserController_AddMoney, "AddMoney" }, // 2035510975
		{ &Z_Construct_UFunction_AToweDefenserController_CanSpendMoney, "CanSpendMoney" }, // 1520715349
		{ &Z_Construct_UFunction_AToweDefenserController_GetCurrency, "GetCurrency" }, // 3142037768
		{ &Z_Construct_UFunction_AToweDefenserController_GetMousePostionFromWorld, "GetMousePostionFromWorld" }, // 354905512
		{ &Z_Construct_UFunction_AToweDefenserController_HandlePlayerState, "HandlePlayerState" }, // 2369554562
		{ &Z_Construct_UFunction_AToweDefenserController_PlaceTower, "PlaceTower" }, // 162844349
		{ &Z_Construct_UFunction_AToweDefenserController_PlayerClick, "PlayerClick" }, // 1716522990
		{ &Z_Construct_UFunction_AToweDefenserController_SetState, "SetState" }, // 3374445789
		{ &Z_Construct_UFunction_AToweDefenserController_StartPlacing, "StartPlacing" }, // 1935670017
		{ &Z_Construct_UFunction_AToweDefenserController_SubtractMoney, "SubtractMoney" }, // 4085016234
		{ &Z_Construct_UFunction_AToweDefenserController_UpdateTowerGhost, "UpdateTowerGhost" }, // 3478312844
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AToweDefenserController_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "ToweDefenserController.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AToweDefenserController_Statics::NewProp_OnSubtractMoney_MetaData[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AToweDefenserController_Statics::NewProp_OnSubtractMoney = { "OnSubtractMoney", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AToweDefenserController, OnSubtractMoney), Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_OnSubtractMoney_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_OnSubtractMoney_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AToweDefenserController_Statics::NewProp_OnAddMoney_MetaData[] = {
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AToweDefenserController_Statics::NewProp_OnAddMoney = { "OnAddMoney", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AToweDefenserController, OnAddMoney), Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_OnAddMoney_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_OnAddMoney_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AToweDefenserController_Statics::NewProp_towerGhostClass_MetaData[] = {
		{ "Category", "ToweDefenserController" },
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AToweDefenserController_Statics::NewProp_towerGhostClass = { "towerGhostClass", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AToweDefenserController, towerGhostClass), Z_Construct_UClass_ATowerGhost_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_towerGhostClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_towerGhostClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AToweDefenserController_Statics::NewProp_HealthComponent_MetaData[] = {
		{ "Category", "ToweDefenserController" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AToweDefenserController_Statics::NewProp_HealthComponent = { "HealthComponent", nullptr, (EPropertyFlags)0x001000000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AToweDefenserController, HealthComponent), Z_Construct_UClass_UHealthComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_HealthComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_HealthComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AToweDefenserController_Statics::NewProp_currency_MetaData[] = {
		{ "Category", "ToweDefenserController" },
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AToweDefenserController_Statics::NewProp_currency = { "currency", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AToweDefenserController, currency), METADATA_PARAMS(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_currency_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_currency_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AToweDefenserController_Statics::NewProp_towerToSpawn_MetaData[] = {
		{ "Category", "ToweDefenserController" },
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AToweDefenserController_Statics::NewProp_towerToSpawn = { "towerToSpawn", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AToweDefenserController, towerToSpawn), Z_Construct_UClass_ATower_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_towerToSpawn_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_towerToSpawn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AToweDefenserController_Statics::NewProp_query_MetaData[] = {
		{ "Category", "ToweDefenserController" },
		{ "ModuleRelativePath", "ToweDefenserController.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AToweDefenserController_Statics::NewProp_query = { "query", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AToweDefenserController, query), Z_Construct_UEnum_Engine_ETraceTypeQuery, METADATA_PARAMS(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_query_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AToweDefenserController_Statics::NewProp_query_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AToweDefenserController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AToweDefenserController_Statics::NewProp_OnSubtractMoney,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AToweDefenserController_Statics::NewProp_OnAddMoney,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AToweDefenserController_Statics::NewProp_towerGhostClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AToweDefenserController_Statics::NewProp_HealthComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AToweDefenserController_Statics::NewProp_currency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AToweDefenserController_Statics::NewProp_towerToSpawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AToweDefenserController_Statics::NewProp_query,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AToweDefenserController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AToweDefenserController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AToweDefenserController_Statics::ClassParams = {
		&AToweDefenserController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AToweDefenserController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AToweDefenserController_Statics::PropPointers),
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AToweDefenserController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AToweDefenserController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AToweDefenserController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AToweDefenserController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AToweDefenserController, 3128011165);
	template<> GAMPRG2_FINAL_API UClass* StaticClass<AToweDefenserController>()
	{
		return AToweDefenserController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AToweDefenserController(Z_Construct_UClass_AToweDefenserController, &AToweDefenserController::StaticClass, TEXT("/Script/gamprg2_final"), TEXT("AToweDefenserController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AToweDefenserController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
