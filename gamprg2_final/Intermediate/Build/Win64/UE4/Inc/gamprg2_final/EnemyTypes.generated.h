// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRG2_FINAL_EnemyTypes_generated_h
#error "EnemyTypes.generated.h already included, missing '#pragma once' in EnemyTypes.h"
#endif
#define GAMPRG2_FINAL_EnemyTypes_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_EnemyTypes_h


#define FOREACH_ENUM_EENEMYGAIT(op) \
	op(EEnemyGait::Grounded) \
	op(EEnemyGait::Flying) 

enum class EEnemyGait : uint8;
template<> GAMPRG2_FINAL_API UEnum* StaticEnum<EEnemyGait>();

#define FOREACH_ENUM_EENEMYTYPES(op) \
	op(EEnemyTypes::Normal) \
	op(EEnemyTypes::Boss) 

enum class EEnemyTypes : uint8;
template<> GAMPRG2_FINAL_API UEnum* StaticEnum<EEnemyTypes>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
