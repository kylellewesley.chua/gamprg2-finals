// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "gamprg2_final/SpawnerInfo.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpawnerInfo() {}
// Cross Module References
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_USpawnerInfo_NoRegister();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_USpawnerInfo();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
	UPackage* Z_Construct_UPackage__Script_gamprg2_final();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_AEnemy_NoRegister();
// End Cross Module References
	void USpawnerInfo::StaticRegisterNativesUSpawnerInfo()
	{
	}
	UClass* Z_Construct_UClass_USpawnerInfo_NoRegister()
	{
		return USpawnerInfo::StaticClass();
	}
	struct Z_Construct_UClass_USpawnerInfo_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_enemyList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_enemyList;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_enemyList_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USpawnerInfo_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_gamprg2_final,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpawnerInfo_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SpawnerInfo.h" },
		{ "ModuleRelativePath", "SpawnerInfo.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpawnerInfo_Statics::NewProp_enemyList_MetaData[] = {
		{ "Category", "SpawnerInfo" },
		{ "ModuleRelativePath", "SpawnerInfo.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USpawnerInfo_Statics::NewProp_enemyList = { "enemyList", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpawnerInfo, enemyList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USpawnerInfo_Statics::NewProp_enemyList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpawnerInfo_Statics::NewProp_enemyList_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_USpawnerInfo_Statics::NewProp_enemyList_Inner = { "enemyList", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AEnemy_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USpawnerInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpawnerInfo_Statics::NewProp_enemyList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpawnerInfo_Statics::NewProp_enemyList_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USpawnerInfo_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USpawnerInfo>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USpawnerInfo_Statics::ClassParams = {
		&USpawnerInfo::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USpawnerInfo_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USpawnerInfo_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USpawnerInfo_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USpawnerInfo_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USpawnerInfo()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USpawnerInfo_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USpawnerInfo, 2332341116);
	template<> GAMPRG2_FINAL_API UClass* StaticClass<USpawnerInfo>()
	{
		return USpawnerInfo::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USpawnerInfo(Z_Construct_UClass_USpawnerInfo, &USpawnerInfo::StaticClass, TEXT("/Script/gamprg2_final"), TEXT("USpawnerInfo"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USpawnerInfo);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
