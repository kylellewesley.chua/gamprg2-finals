// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "gamprg2_final/TowerPlacement.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerPlacement() {}
// Cross Module References
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_ATowerPlacement_NoRegister();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_ATowerPlacement();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_gamprg2_final();
	ENGINE_API UClass* Z_Construct_UClass_UArrowComponent_NoRegister();
// End Cross Module References
	void ATowerPlacement::StaticRegisterNativesATowerPlacement()
	{
	}
	UClass* Z_Construct_UClass_ATowerPlacement_NoRegister()
	{
		return ATowerPlacement::StaticClass();
	}
	struct Z_Construct_UClass_ATowerPlacement_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Arrow_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Arrow;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATowerPlacement_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_gamprg2_final,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerPlacement_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "TowerPlacement.h" },
		{ "ModuleRelativePath", "TowerPlacement.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerPlacement_Statics::NewProp_Arrow_MetaData[] = {
		{ "Category", "TowerPlacement" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TowerPlacement.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATowerPlacement_Statics::NewProp_Arrow = { "Arrow", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATowerPlacement, Arrow), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATowerPlacement_Statics::NewProp_Arrow_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerPlacement_Statics::NewProp_Arrow_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATowerPlacement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATowerPlacement_Statics::NewProp_Arrow,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATowerPlacement_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATowerPlacement>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATowerPlacement_Statics::ClassParams = {
		&ATowerPlacement::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ATowerPlacement_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ATowerPlacement_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATowerPlacement_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerPlacement_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATowerPlacement()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATowerPlacement_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATowerPlacement, 3933291811);
	template<> GAMPRG2_FINAL_API UClass* StaticClass<ATowerPlacement>()
	{
		return ATowerPlacement::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATowerPlacement(Z_Construct_UClass_ATowerPlacement, &ATowerPlacement::StaticClass, TEXT("/Script/gamprg2_final"), TEXT("ATowerPlacement"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATowerPlacement);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
