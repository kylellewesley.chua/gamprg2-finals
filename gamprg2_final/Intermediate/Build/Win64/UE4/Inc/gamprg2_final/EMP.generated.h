// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef GAMPRG2_FINAL_EMP_generated_h
#error "EMP.generated.h already included, missing '#pragma once' in EMP.h"
#endif
#define GAMPRG2_FINAL_EMP_generated_h

#define gamprg2_final_Source_gamprg2_final_EMP_h_19_SPARSE_DATA
#define gamprg2_final_Source_gamprg2_final_EMP_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execEndOverlap); \
	DECLARE_FUNCTION(execOnOverlap);


#define gamprg2_final_Source_gamprg2_final_EMP_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execEndOverlap); \
	DECLARE_FUNCTION(execOnOverlap);


#define gamprg2_final_Source_gamprg2_final_EMP_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEMP(); \
	friend struct Z_Construct_UClass_AEMP_Statics; \
public: \
	DECLARE_CLASS(AEMP, ATower, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(AEMP)


#define gamprg2_final_Source_gamprg2_final_EMP_h_19_INCLASS \
private: \
	static void StaticRegisterNativesAEMP(); \
	friend struct Z_Construct_UClass_AEMP_Statics; \
public: \
	DECLARE_CLASS(AEMP, ATower, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(AEMP)


#define gamprg2_final_Source_gamprg2_final_EMP_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEMP(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEMP) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEMP); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEMP); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEMP(AEMP&&); \
	NO_API AEMP(const AEMP&); \
public:


#define gamprg2_final_Source_gamprg2_final_EMP_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEMP(AEMP&&); \
	NO_API AEMP(const AEMP&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEMP); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEMP); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEMP)


#define gamprg2_final_Source_gamprg2_final_EMP_h_19_PRIVATE_PROPERTY_OFFSET
#define gamprg2_final_Source_gamprg2_final_EMP_h_16_PROLOG
#define gamprg2_final_Source_gamprg2_final_EMP_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_EMP_h_19_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_EMP_h_19_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_EMP_h_19_RPC_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_EMP_h_19_INCLASS \
	gamprg2_final_Source_gamprg2_final_EMP_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gamprg2_final_Source_gamprg2_final_EMP_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_EMP_h_19_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_EMP_h_19_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_EMP_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_EMP_h_19_INCLASS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_EMP_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPRG2_FINAL_API UClass* StaticClass<class AEMP>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_EMP_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
