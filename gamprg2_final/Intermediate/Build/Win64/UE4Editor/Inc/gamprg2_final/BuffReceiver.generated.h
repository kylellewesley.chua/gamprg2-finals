// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UBuff;
#ifdef GAMPRG2_FINAL_BuffReceiver_generated_h
#error "BuffReceiver.generated.h already included, missing '#pragma once' in BuffReceiver.h"
#endif
#define GAMPRG2_FINAL_BuffReceiver_generated_h

#define gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_SPARSE_DATA
#define gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetBuff); \
	DECLARE_FUNCTION(execContainsBuff); \
	DECLARE_FUNCTION(execRemoveBuff); \
	DECLARE_FUNCTION(execAddBuff);


#define gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetBuff); \
	DECLARE_FUNCTION(execContainsBuff); \
	DECLARE_FUNCTION(execRemoveBuff); \
	DECLARE_FUNCTION(execAddBuff);


#define gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBuffReceiver(); \
	friend struct Z_Construct_UClass_UBuffReceiver_Statics; \
public: \
	DECLARE_CLASS(UBuffReceiver, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(UBuffReceiver)


#define gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUBuffReceiver(); \
	friend struct Z_Construct_UClass_UBuffReceiver_Statics; \
public: \
	DECLARE_CLASS(UBuffReceiver, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(UBuffReceiver)


#define gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBuffReceiver(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBuffReceiver) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBuffReceiver); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBuffReceiver); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBuffReceiver(UBuffReceiver&&); \
	NO_API UBuffReceiver(const UBuffReceiver&); \
public:


#define gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBuffReceiver(UBuffReceiver&&); \
	NO_API UBuffReceiver(const UBuffReceiver&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBuffReceiver); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBuffReceiver); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBuffReceiver)


#define gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_PRIVATE_PROPERTY_OFFSET
#define gamprg2_final_Source_gamprg2_final_BuffReceiver_h_11_PROLOG
#define gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_RPC_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_INCLASS \
	gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_INCLASS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_BuffReceiver_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPRG2_FINAL_API UClass* StaticClass<class UBuffReceiver>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_BuffReceiver_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
