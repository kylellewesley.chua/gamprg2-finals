// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "gamprg2_final/TowerDefenseGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerDefenseGameMode() {}
// Cross Module References
	GAMPRG2_FINAL_API UFunction* Z_Construct_UDelegateFunction_gamprg2_final_OnEndWaveSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_gamprg2_final();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_ATowerDefenseGameMode_NoRegister();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_ATowerDefenseGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_gamprg2_final_OnEndWaveSignature__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_gamprg2_final_OnEndWaveSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenseGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_gamprg2_final_OnEndWaveSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_gamprg2_final, nullptr, "OnEndWaveSignature__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_gamprg2_final_OnEndWaveSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_gamprg2_final_OnEndWaveSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_gamprg2_final_OnEndWaveSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_gamprg2_final_OnEndWaveSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(ATowerDefenseGameMode::execWaveCount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->WaveCount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATowerDefenseGameMode::execStartWave)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartWave();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATowerDefenseGameMode::execGetCurrentWave)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetCurrentWave();
		P_NATIVE_END;
	}
	void ATowerDefenseGameMode::StaticRegisterNativesATowerDefenseGameMode()
	{
		UClass* Class = ATowerDefenseGameMode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCurrentWave", &ATowerDefenseGameMode::execGetCurrentWave },
			{ "StartWave", &ATowerDefenseGameMode::execStartWave },
			{ "WaveCount", &ATowerDefenseGameMode::execWaveCount },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave_Statics
	{
		struct TowerDefenseGameMode_eventGetCurrentWave_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TowerDefenseGameMode_eventGetCurrentWave_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenseGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenseGameMode, nullptr, "GetCurrentWave", nullptr, nullptr, sizeof(TowerDefenseGameMode_eventGetCurrentWave_Parms), Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenseGameMode_StartWave_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenseGameMode_StartWave_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenseGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenseGameMode_StartWave_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenseGameMode, nullptr, "StartWave", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenseGameMode_StartWave_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenseGameMode_StartWave_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenseGameMode_StartWave()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenseGameMode_StartWave_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATowerDefenseGameMode_WaveCount_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATowerDefenseGameMode_WaveCount_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TowerDefenseGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATowerDefenseGameMode_WaveCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATowerDefenseGameMode, nullptr, "WaveCount", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATowerDefenseGameMode_WaveCount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATowerDefenseGameMode_WaveCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATowerDefenseGameMode_WaveCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATowerDefenseGameMode_WaveCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATowerDefenseGameMode_NoRegister()
	{
		return ATowerDefenseGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ATowerDefenseGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnStartWave_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnStartWave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnEndWave_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnEndWave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_waveDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_waveDelay;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATowerDefenseGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_gamprg2_final,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATowerDefenseGameMode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATowerDefenseGameMode_GetCurrentWave, "GetCurrentWave" }, // 1744095033
		{ &Z_Construct_UFunction_ATowerDefenseGameMode_StartWave, "StartWave" }, // 1595289799
		{ &Z_Construct_UFunction_ATowerDefenseGameMode_WaveCount, "WaveCount" }, // 3686360710
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerDefenseGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TowerDefenseGameMode.h" },
		{ "ModuleRelativePath", "TowerDefenseGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_OnStartWave_MetaData[] = {
		{ "ModuleRelativePath", "TowerDefenseGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_OnStartWave = { "OnStartWave", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATowerDefenseGameMode, OnStartWave), Z_Construct_UDelegateFunction_gamprg2_final_OnEndWaveSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_OnStartWave_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_OnStartWave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_OnEndWave_MetaData[] = {
		{ "ModuleRelativePath", "TowerDefenseGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_OnEndWave = { "OnEndWave", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATowerDefenseGameMode, OnEndWave), Z_Construct_UDelegateFunction_gamprg2_final_OnEndWaveSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_OnEndWave_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_OnEndWave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_waveDelay_MetaData[] = {
		{ "Category", "TowerDefenseGameMode" },
		{ "ModuleRelativePath", "TowerDefenseGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_waveDelay = { "waveDelay", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATowerDefenseGameMode, waveDelay), METADATA_PARAMS(Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_waveDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_waveDelay_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATowerDefenseGameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_OnStartWave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_OnEndWave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATowerDefenseGameMode_Statics::NewProp_waveDelay,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATowerDefenseGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATowerDefenseGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATowerDefenseGameMode_Statics::ClassParams = {
		&ATowerDefenseGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ATowerDefenseGameMode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ATowerDefenseGameMode_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ATowerDefenseGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerDefenseGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATowerDefenseGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATowerDefenseGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATowerDefenseGameMode, 2759483358);
	template<> GAMPRG2_FINAL_API UClass* StaticClass<ATowerDefenseGameMode>()
	{
		return ATowerDefenseGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATowerDefenseGameMode(Z_Construct_UClass_ATowerDefenseGameMode, &ATowerDefenseGameMode::StaticClass, TEXT("/Script/gamprg2_final"), TEXT("ATowerDefenseGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATowerDefenseGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
