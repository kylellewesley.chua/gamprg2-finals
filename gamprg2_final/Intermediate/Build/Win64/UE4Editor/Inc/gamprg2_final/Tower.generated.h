// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
class AEnemy;
#ifdef GAMPRG2_FINAL_Tower_generated_h
#error "Tower.generated.h already included, missing '#pragma once' in Tower.h"
#endif
#define GAMPRG2_FINAL_Tower_generated_h

#define gamprg2_final_Source_gamprg2_final_Tower_h_20_SPARSE_DATA
#define gamprg2_final_Source_gamprg2_final_Tower_h_20_RPC_WRAPPERS \
	virtual void ShootProjectile_Implementation(FVector location); \
 \
	DECLARE_FUNCTION(execCheckForEnemy); \
	DECLARE_FUNCTION(execShootProjectile); \
	DECLARE_FUNCTION(execTargetEnemy);


#define gamprg2_final_Source_gamprg2_final_Tower_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCheckForEnemy); \
	DECLARE_FUNCTION(execShootProjectile); \
	DECLARE_FUNCTION(execTargetEnemy);


#define gamprg2_final_Source_gamprg2_final_Tower_h_20_EVENT_PARMS \
	struct Tower_eventShootProjectile_Parms \
	{ \
		FVector location; \
	};


#define gamprg2_final_Source_gamprg2_final_Tower_h_20_CALLBACK_WRAPPERS
#define gamprg2_final_Source_gamprg2_final_Tower_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATower(); \
	friend struct Z_Construct_UClass_ATower_Statics; \
public: \
	DECLARE_CLASS(ATower, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(ATower)


#define gamprg2_final_Source_gamprg2_final_Tower_h_20_INCLASS \
private: \
	static void StaticRegisterNativesATower(); \
	friend struct Z_Construct_UClass_ATower_Statics; \
public: \
	DECLARE_CLASS(ATower, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(ATower)


#define gamprg2_final_Source_gamprg2_final_Tower_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATower(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATower) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATower); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATower); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATower(ATower&&); \
	NO_API ATower(const ATower&); \
public:


#define gamprg2_final_Source_gamprg2_final_Tower_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATower(ATower&&); \
	NO_API ATower(const ATower&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATower); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATower); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATower)


#define gamprg2_final_Source_gamprg2_final_Tower_h_20_PRIVATE_PROPERTY_OFFSET
#define gamprg2_final_Source_gamprg2_final_Tower_h_17_PROLOG \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_EVENT_PARMS


#define gamprg2_final_Source_gamprg2_final_Tower_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_RPC_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_CALLBACK_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_INCLASS \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gamprg2_final_Source_gamprg2_final_Tower_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_CALLBACK_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_INCLASS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_Tower_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPRG2_FINAL_API UClass* StaticClass<class ATower>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_Tower_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
