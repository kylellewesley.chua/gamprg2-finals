// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "gamprg2_final/TowerGhost.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerGhost() {}
// Cross Module References
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_ATowerGhost_NoRegister();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_ATowerGhost();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_gamprg2_final();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void ATowerGhost::StaticRegisterNativesATowerGhost()
	{
	}
	UClass* Z_Construct_UClass_ATowerGhost_NoRegister()
	{
		return ATowerGhost::StaticClass();
	}
	struct Z_Construct_UClass_ATowerGhost_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshTurretHead_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshTurretHead;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshTurretBase_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshTurretBase;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATowerGhost_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_gamprg2_final,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerGhost_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "TowerGhost.h" },
		{ "ModuleRelativePath", "TowerGhost.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerGhost_Statics::NewProp_StaticMeshTurretHead_MetaData[] = {
		{ "Category", "TowerGhost" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TowerGhost.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATowerGhost_Statics::NewProp_StaticMeshTurretHead = { "StaticMeshTurretHead", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATowerGhost, StaticMeshTurretHead), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATowerGhost_Statics::NewProp_StaticMeshTurretHead_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerGhost_Statics::NewProp_StaticMeshTurretHead_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATowerGhost_Statics::NewProp_StaticMeshTurretBase_MetaData[] = {
		{ "Category", "TowerGhost" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TowerGhost.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATowerGhost_Statics::NewProp_StaticMeshTurretBase = { "StaticMeshTurretBase", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATowerGhost, StaticMeshTurretBase), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATowerGhost_Statics::NewProp_StaticMeshTurretBase_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerGhost_Statics::NewProp_StaticMeshTurretBase_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATowerGhost_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATowerGhost_Statics::NewProp_StaticMeshTurretHead,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATowerGhost_Statics::NewProp_StaticMeshTurretBase,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATowerGhost_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATowerGhost>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATowerGhost_Statics::ClassParams = {
		&ATowerGhost::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ATowerGhost_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ATowerGhost_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATowerGhost_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATowerGhost_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATowerGhost()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATowerGhost_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATowerGhost, 211695552);
	template<> GAMPRG2_FINAL_API UClass* StaticClass<ATowerGhost>()
	{
		return ATowerGhost::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATowerGhost(Z_Construct_UClass_ATowerGhost, &ATowerGhost::StaticClass, TEXT("/Script/gamprg2_final"), TEXT("ATowerGhost"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATowerGhost);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
