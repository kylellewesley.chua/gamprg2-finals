// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
class AEnemy;
#ifdef GAMPRG2_FINAL_PlayerCore_generated_h
#error "PlayerCore.generated.h already included, missing '#pragma once' in PlayerCore.h"
#endif
#define GAMPRG2_FINAL_PlayerCore_generated_h

#define gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_SPARSE_DATA
#define gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit); \
	DECLARE_FUNCTION(execCoreCollision);


#define gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit); \
	DECLARE_FUNCTION(execCoreCollision);


#define gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerCore(); \
	friend struct Z_Construct_UClass_APlayerCore_Statics; \
public: \
	DECLARE_CLASS(APlayerCore, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(APlayerCore)


#define gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerCore(); \
	friend struct Z_Construct_UClass_APlayerCore_Statics; \
public: \
	DECLARE_CLASS(APlayerCore, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(APlayerCore)


#define gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerCore(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerCore) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerCore); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerCore); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerCore(APlayerCore&&); \
	NO_API APlayerCore(const APlayerCore&); \
public:


#define gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerCore(APlayerCore&&); \
	NO_API APlayerCore(const APlayerCore&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerCore); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerCore); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerCore)


#define gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_PRIVATE_PROPERTY_OFFSET
#define gamprg2_final_Source_gamprg2_final_PlayerCore_h_13_PROLOG
#define gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_RPC_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_INCLASS \
	gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_INCLASS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_PlayerCore_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPRG2_FINAL_API UClass* StaticClass<class APlayerCore>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_PlayerCore_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
