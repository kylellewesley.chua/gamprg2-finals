// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodegamprg2_final_init() {}
	GAMPRG2_FINAL_API UFunction* Z_Construct_UDelegateFunction_gamprg2_final_OnDeath__DelegateSignature();
	GAMPRG2_FINAL_API UFunction* Z_Construct_UDelegateFunction_gamprg2_final_OnDieSignature__DelegateSignature();
	GAMPRG2_FINAL_API UFunction* Z_Construct_UDelegateFunction_gamprg2_final_OnClearWave__DelegateSignature();
	GAMPRG2_FINAL_API UFunction* Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature();
	GAMPRG2_FINAL_API UFunction* Z_Construct_UDelegateFunction_gamprg2_final_OnEndWaveSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_gamprg2_final()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_gamprg2_final_OnDeath__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_gamprg2_final_OnDieSignature__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_gamprg2_final_OnClearWave__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_gamprg2_final_MoneySignature__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_gamprg2_final_OnEndWaveSignature__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/gamprg2_final",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x7656370B,
				0xE89ADB36,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
