// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "gamprg2_final/LaserTower.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLaserTower() {}
// Cross Module References
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_ALaserTower_NoRegister();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_ALaserTower();
	GAMPRG2_FINAL_API UClass* Z_Construct_UClass_ATower();
	UPackage* Z_Construct_UPackage__Script_gamprg2_final();
// End Cross Module References
	void ALaserTower::StaticRegisterNativesALaserTower()
	{
	}
	UClass* Z_Construct_UClass_ALaserTower_NoRegister()
	{
		return ALaserTower::StaticClass();
	}
	struct Z_Construct_UClass_ALaserTower_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALaserTower_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ATower,
		(UObject* (*)())Z_Construct_UPackage__Script_gamprg2_final,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaserTower_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "LaserTower.h" },
		{ "ModuleRelativePath", "LaserTower.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALaserTower_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALaserTower>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALaserTower_Statics::ClassParams = {
		&ALaserTower::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ALaserTower_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALaserTower_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALaserTower()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALaserTower_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALaserTower, 1184649963);
	template<> GAMPRG2_FINAL_API UClass* StaticClass<ALaserTower>()
	{
		return ALaserTower::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALaserTower(Z_Construct_UClass_ALaserTower, &ALaserTower::StaticClass, TEXT("/Script/gamprg2_final"), TEXT("ALaserTower"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALaserTower);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
