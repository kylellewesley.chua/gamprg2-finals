// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRG2_FINAL_TowerDefenseGameMode_generated_h
#error "TowerDefenseGameMode.generated.h already included, missing '#pragma once' in TowerDefenseGameMode.h"
#endif
#define GAMPRG2_FINAL_TowerDefenseGameMode_generated_h

#define gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_14_DELEGATE \
static inline void FOnEndWaveSignature_DelegateWrapper(const FMulticastScriptDelegate& OnEndWaveSignature) \
{ \
	OnEndWaveSignature.ProcessMulticastDelegate<UObject>(NULL); \
}


#define gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_SPARSE_DATA
#define gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execWaveCount); \
	DECLARE_FUNCTION(execStartWave); \
	DECLARE_FUNCTION(execGetCurrentWave);


#define gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execWaveCount); \
	DECLARE_FUNCTION(execStartWave); \
	DECLARE_FUNCTION(execGetCurrentWave);


#define gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATowerDefenseGameMode(); \
	friend struct Z_Construct_UClass_ATowerDefenseGameMode_Statics; \
public: \
	DECLARE_CLASS(ATowerDefenseGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(ATowerDefenseGameMode)


#define gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_INCLASS \
private: \
	static void StaticRegisterNativesATowerDefenseGameMode(); \
	friend struct Z_Construct_UClass_ATowerDefenseGameMode_Statics; \
public: \
	DECLARE_CLASS(ATowerDefenseGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(ATowerDefenseGameMode)


#define gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATowerDefenseGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATowerDefenseGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerDefenseGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerDefenseGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerDefenseGameMode(ATowerDefenseGameMode&&); \
	NO_API ATowerDefenseGameMode(const ATowerDefenseGameMode&); \
public:


#define gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATowerDefenseGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerDefenseGameMode(ATowerDefenseGameMode&&); \
	NO_API ATowerDefenseGameMode(const ATowerDefenseGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerDefenseGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerDefenseGameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATowerDefenseGameMode)


#define gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_PRIVATE_PROPERTY_OFFSET
#define gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_15_PROLOG
#define gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_RPC_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_INCLASS \
	gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_INCLASS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPRG2_FINAL_API UClass* StaticClass<class ATowerDefenseGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_TowerDefenseGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
