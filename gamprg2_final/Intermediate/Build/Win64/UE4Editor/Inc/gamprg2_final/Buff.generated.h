// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UBuffReceiver;
#ifdef GAMPRG2_FINAL_Buff_generated_h
#error "Buff.generated.h already included, missing '#pragma once' in Buff.h"
#endif
#define GAMPRG2_FINAL_Buff_generated_h

#define gamprg2_final_Source_gamprg2_final_Buff_h_18_SPARSE_DATA
#define gamprg2_final_Source_gamprg2_final_Buff_h_18_RPC_WRAPPERS \
	virtual void Tick_Implementation(UBuffReceiver* buffreceiver, float DeltaTime); \
	virtual void OnRemoveBuff_Implementation(UBuffReceiver* buffreceiver); \
	virtual void OnReceiveBuff_Implementation(UBuffReceiver* buffreceiver); \
 \
	DECLARE_FUNCTION(execTick); \
	DECLARE_FUNCTION(execOnRemoveBuff); \
	DECLARE_FUNCTION(execOnReceiveBuff);


#define gamprg2_final_Source_gamprg2_final_Buff_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTick); \
	DECLARE_FUNCTION(execOnRemoveBuff); \
	DECLARE_FUNCTION(execOnReceiveBuff);


#define gamprg2_final_Source_gamprg2_final_Buff_h_18_EVENT_PARMS \
	struct Buff_eventOnReceiveBuff_Parms \
	{ \
		UBuffReceiver* buffreceiver; \
	}; \
	struct Buff_eventOnRemoveBuff_Parms \
	{ \
		UBuffReceiver* buffreceiver; \
	}; \
	struct Buff_eventTick_Parms \
	{ \
		UBuffReceiver* buffreceiver; \
		float DeltaTime; \
	};


#define gamprg2_final_Source_gamprg2_final_Buff_h_18_CALLBACK_WRAPPERS
#define gamprg2_final_Source_gamprg2_final_Buff_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBuff(); \
	friend struct Z_Construct_UClass_UBuff_Statics; \
public: \
	DECLARE_CLASS(UBuff, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(UBuff)


#define gamprg2_final_Source_gamprg2_final_Buff_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUBuff(); \
	friend struct Z_Construct_UClass_UBuff_Statics; \
public: \
	DECLARE_CLASS(UBuff, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(UBuff)


#define gamprg2_final_Source_gamprg2_final_Buff_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBuff(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBuff) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBuff); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBuff); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBuff(UBuff&&); \
	NO_API UBuff(const UBuff&); \
public:


#define gamprg2_final_Source_gamprg2_final_Buff_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBuff(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBuff(UBuff&&); \
	NO_API UBuff(const UBuff&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBuff); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBuff); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBuff)


#define gamprg2_final_Source_gamprg2_final_Buff_h_18_PRIVATE_PROPERTY_OFFSET
#define gamprg2_final_Source_gamprg2_final_Buff_h_15_PROLOG \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_EVENT_PARMS


#define gamprg2_final_Source_gamprg2_final_Buff_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_RPC_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_CALLBACK_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_INCLASS \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gamprg2_final_Source_gamprg2_final_Buff_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_CALLBACK_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_INCLASS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_Buff_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPRG2_FINAL_API UClass* StaticClass<class UBuff>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_Buff_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
