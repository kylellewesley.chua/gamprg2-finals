// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRG2_FINAL_SpawnerInfo_generated_h
#error "SpawnerInfo.generated.h already included, missing '#pragma once' in SpawnerInfo.h"
#endif
#define GAMPRG2_FINAL_SpawnerInfo_generated_h

#define gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_SPARSE_DATA
#define gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_RPC_WRAPPERS
#define gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSpawnerInfo(); \
	friend struct Z_Construct_UClass_USpawnerInfo_Statics; \
public: \
	DECLARE_CLASS(USpawnerInfo, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(USpawnerInfo)


#define gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUSpawnerInfo(); \
	friend struct Z_Construct_UClass_USpawnerInfo_Statics; \
public: \
	DECLARE_CLASS(USpawnerInfo, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(USpawnerInfo)


#define gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpawnerInfo(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpawnerInfo) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpawnerInfo); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpawnerInfo); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpawnerInfo(USpawnerInfo&&); \
	NO_API USpawnerInfo(const USpawnerInfo&); \
public:


#define gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpawnerInfo(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpawnerInfo(USpawnerInfo&&); \
	NO_API USpawnerInfo(const USpawnerInfo&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpawnerInfo); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpawnerInfo); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpawnerInfo)


#define gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_PRIVATE_PROPERTY_OFFSET
#define gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_16_PROLOG
#define gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_RPC_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_INCLASS \
	gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_INCLASS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_SpawnerInfo_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPRG2_FINAL_API UClass* StaticClass<class USpawnerInfo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_SpawnerInfo_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
