// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRG2_FINAL_BuffDuration_generated_h
#error "BuffDuration.generated.h already included, missing '#pragma once' in BuffDuration.h"
#endif
#define GAMPRG2_FINAL_BuffDuration_generated_h

#define gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_SPARSE_DATA
#define gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execResetDuration);


#define gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execResetDuration);


#define gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBuffDuration(); \
	friend struct Z_Construct_UClass_UBuffDuration_Statics; \
public: \
	DECLARE_CLASS(UBuffDuration, UBuff, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(UBuffDuration)


#define gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBuffDuration(); \
	friend struct Z_Construct_UClass_UBuffDuration_Statics; \
public: \
	DECLARE_CLASS(UBuffDuration, UBuff, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/gamprg2_final"), NO_API) \
	DECLARE_SERIALIZER(UBuffDuration)


#define gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBuffDuration(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBuffDuration) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBuffDuration); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBuffDuration); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBuffDuration(UBuffDuration&&); \
	NO_API UBuffDuration(const UBuffDuration&); \
public:


#define gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBuffDuration(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBuffDuration(UBuffDuration&&); \
	NO_API UBuffDuration(const UBuffDuration&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBuffDuration); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBuffDuration); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBuffDuration)


#define gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_PRIVATE_PROPERTY_OFFSET
#define gamprg2_final_Source_gamprg2_final_BuffDuration_h_12_PROLOG
#define gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_RPC_WRAPPERS \
	gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_INCLASS \
	gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_PRIVATE_PROPERTY_OFFSET \
	gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_SPARSE_DATA \
	gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_INCLASS_NO_PURE_DECLS \
	gamprg2_final_Source_gamprg2_final_BuffDuration_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPRG2_FINAL_API UClass* StaticClass<class UBuffDuration>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_BuffDuration_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
