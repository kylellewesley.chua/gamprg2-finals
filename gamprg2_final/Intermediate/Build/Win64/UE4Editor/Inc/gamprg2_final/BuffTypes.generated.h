// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPRG2_FINAL_BuffTypes_generated_h
#error "BuffTypes.generated.h already included, missing '#pragma once' in BuffTypes.h"
#endif
#define GAMPRG2_FINAL_BuffTypes_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gamprg2_final_Source_gamprg2_final_BuffTypes_h


#define FOREACH_ENUM_EBUFFTYPES(op) \
	op(EBuffTypes::DURATION) \
	op(EBuffTypes::PERMANENT) 

enum class EBuffTypes : uint8;
template<> GAMPRG2_FINAL_API UEnum* StaticEnum<EBuffTypes>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
